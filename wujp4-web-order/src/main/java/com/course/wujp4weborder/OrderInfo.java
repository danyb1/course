/**
 * 
 */
package com.course.wujp4weborder;

import lombok.Data;

/**
 * @author jojo
 *
 */
@Data
public class OrderInfo {
	
	private Long id;
	
	private Long productId;
	
}
