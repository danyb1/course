package com.yzp;

import lombok.Data;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/22 15:48
 */
@Data
public class OrderInfo {
    private long id;
    private long productId;
}
