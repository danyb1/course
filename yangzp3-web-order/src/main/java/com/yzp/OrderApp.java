package com.yzp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/22 15:48
 */
@SpringBootApplication
@Slf4j
public class OrderApp {
    public static void main(String[] args) {
        Environment env = SpringApplication.run(OrderApp.class, args).getEnvironment();
        log.info("Order App 已启动，端口号："+env.getProperty("server.port"));
    }
}
