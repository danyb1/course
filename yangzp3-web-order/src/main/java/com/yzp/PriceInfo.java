package com.yzp;

import lombok.Data;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.math.BigDecimal;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/22 15:48
 */
@Data
public class PriceInfo {
    private long id;
    private BigDecimal price;
}
