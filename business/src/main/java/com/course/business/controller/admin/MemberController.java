package com.course.business.controller.admin;

import com.course.server.dto.MemberDto;
import com.course.server.dto.PageDto;
import com.course.server.dto.ResponseDto;
import com.course.server.service.MemberService;
import com.course.server.util.ValidatorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author danyu
 */
@RestController
@RequestMapping("/admin/member")
public class MemberController {

    private static final Logger LOG = LoggerFactory.getLogger(MemberController.class);
    public static final String BUSINESS_NAME = "会员";

    @Resource
    MemberService memberService;

    /**
     * 列表查询
     */
    @PostMapping("/list")
    public ResponseDto member(@RequestBody PageDto pageDto) {
        memberService.list(pageDto);
        ResponseDto responseDto = new ResponseDto();
        responseDto.setContent(pageDto);
        return responseDto;
    }

    /**
     * 保存，id有值时更新，无值时新增
     */
//    @PostMapping("/save")
//    public ResponseDto save(@RequestBody MemberDto memberDto) {
//        // 保存校验
//        ValidatorUtil.length(memberDto.getMobile(), "手机号", 1, 11);
//        ValidatorUtil.require(memberDto.getPassword(), "密码");
//        ValidatorUtil.length(memberDto.getName(), "昵称", 1, 50);
//        ValidatorUtil.length(memberDto.getPhoto(), "头像url", 1, 200);
//
//        memberService.save(memberDto);
//        ResponseDto responseDto = new ResponseDto();
//        responseDto.setContent(memberDto);
//        return responseDto;
//    }
//
//    /**
//     * 删除
//     */
//    @DeleteMapping("/delete/{id}")
//    public ResponseDto delete(@PathVariable String id) {
//        ResponseDto responseDto = new ResponseDto();
//        memberService.delete(id);
//        return responseDto;
//    }
}
