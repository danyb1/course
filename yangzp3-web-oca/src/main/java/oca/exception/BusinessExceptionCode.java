package oca.exception;

public enum BusinessExceptionCode {

    LOGIN_ERROR("10000","登录异常"),
    MEMBER_NOT_EXIST("10001","会员不存在"),
    USER_LOGIN_NAME_EXIST("10002","登录名已存在"),
    LOGIN_USER_ERROR("10003","用户名不存在或密码错误"),
    LOGIN_MEMBER_ERROR("10004","手机号不存在或密码错误"),
    MOBILE_CODE_TOO_FREQUENT("10005","短信请求过于频繁"),
    MOBILE_CODE_ERROR("10006","短信验证码不正确"),
    MOBILE_CODE_EXPIRED("10007","短信验证码不存在或已过期，请重新发送短信"),
    ;

    private String code;
    private String desc;

    BusinessExceptionCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
