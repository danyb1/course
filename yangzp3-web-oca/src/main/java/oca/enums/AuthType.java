package oca.enums;

import org.apache.commons.lang.StringUtils;

public enum AuthType {
    BEFORE("1"), AFTER("2"), ERROR("3"), SUCCESS("4"), PASSWD("5");
    private String value;
    AuthType(String value){
        this.value = value;
    }
    public String getValue(){
        return this.value;
    }
}
