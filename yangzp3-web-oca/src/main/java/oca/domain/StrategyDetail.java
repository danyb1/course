package oca.domain;

import lombok.Data;

@Data
public class StrategyDetail {
    private String crelId;
    private String sysId;
    private String crelName;
    private String enableFlag;
    private String crelDetail;
    private String actionType;
    private String authType;

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getCrelId() {
        return crelId;
    }

    public void setCrelId(String crelId) {
        this.crelId = crelId;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getCerlName() {
        return crelName;
    }

    public void setCerlName(String cerlName) {
        this.crelName = cerlName;
    }

    public String getEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(String enableFlag) {
        this.enableFlag = enableFlag;
    }

    public String getCrelDetail() {
        return crelDetail;
    }

    public void setCrelDetail(String crelDetail) {
        this.crelDetail = crelDetail;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
}
