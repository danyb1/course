package oca.dto;

import java.util.List;

public class UserLoginRepDto {
    /**
     * 登录结果
     */
    private Boolean result ;

    private String code ;

    private String message;

    //private String authCode = "";

    /**
     * 策略验证结果信息
     */


    private List<StrategyMessageDto> strategyMessage;

    public Boolean getResult() {
        return result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /*public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }*/

    public void setResult(Boolean result) {
        this.result = result;
    }

    public List<StrategyMessageDto> getStrategyMessage() {
        return strategyMessage;
    }

    public void setStrategyMessage(List<StrategyMessageDto> strategyMessage) {
        this.strategyMessage = strategyMessage;
    }
}
