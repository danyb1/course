package oca.service.auth.impl;


import oca.dto.UserLoginReqDto;
import oca.exception.BusinessException;
import oca.exception.BusinessExceptionCode;
import oca.service.auth.IUserAuthService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 密码安全验证方式实现
 */
@Component("passwordUserAuthService")
public class PasswordIUserAuthServiceImpl implements IUserAuthService {

    @Override
    public String getLoginCode(UserLoginReqDto userDto) throws BusinessException {
        if(StringUtils.isEmpty(userDto.getLoginCode())){
            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
        }
        return userDto.getLoginCode();
    }

    @Override
    public boolean auth(UserLoginReqDto userDto) {
        return true;
    }

    @Override
    public void authFailed(UserLoginReqDto userDto) throws BusinessException {
        throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
    }
}
