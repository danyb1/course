package oca.service.strategy;


import oca.domain.StrategyDetail;
import oca.domain.UserDetail;
import oca.dto.StrategyMessageDto;
import oca.dto.UserLoginReqDto;
import oca.enums.AuthType;

/**
 * 认证策略定义
 */
public interface IStrategyDefineService {
    /**
     * 策略验证
     * @param reqDto
     * @param userDetail
     * @return
     */
    public StrategyMessageDto validate(UserLoginReqDto reqDto, UserDetail userDetail);

    /**
     * 认证类型
     * @return
     */
    public AuthType getAuthType();

    /**
     * 认证策略名称
     * @return
     */
    public String getStrategyName();

    /**
     * 策略初始化
     * @param stra
     */
    public void initStrategy(StrategyDetail stra);

    /**
     * 策略是否开启
     * @return
     */
    public boolean isEnable();
}
