package oca.service.strategy.impl;

import oca.domain.StrategyDetail;
import oca.enums.AuthType;
import oca.mapper.StrategyDetailMapper;
import oca.service.strategy.IStrategyDefineService;
import oca.service.strategy.IStrategyInitService;
import oca.util.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class StrategyInitServiceImpl implements IStrategyInitService {
    //下面的第一个String表示的是sysId
    private ConcurrentHashMap<String, Map<AuthType, List<IStrategyDefineService>>> strategyDefineServiceMap = new ConcurrentHashMap<>();

    private static ConcurrentHashMap<AuthType, List<IStrategyDefineService>> strategyMap = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Long> creStrategyInitTimeMap = new ConcurrentHashMap<>();
    private final static Long CHANGE_TIME = 60000L;
    private static Logger LOGGER = LoggerFactory.getLogger(StrategyInitServiceImpl.class);

    static {
        strategyMap.put(AuthType.BEFORE,new ArrayList<>());
        strategyMap.put(AuthType.AFTER,new ArrayList<>());
        strategyMap.put(AuthType.ERROR,new ArrayList<>());
        strategyMap.put(AuthType.SUCCESS,new ArrayList<>());
        strategyMap.put(AuthType.PASSWD,new ArrayList<>());
    }

    @Autowired
    private StrategyDetailMapper strategyDetailMapper ;  //要是不能导入，则直接在本类上面@Import(StrategyDetailMapper.class)

    @Override
    public Map<AuthType, List<IStrategyDefineService>> initStrategy(String sysId) {
    //public Map<AuthType, ArrayList<StrategyDetail>> initStrategy(String sysId) {
        //根据系统ID初始化认证策略
        //认证策略设置一定缓存时间
        //认证策略加载时，根据认证策略名称与实现类getStrategyName返回值匹配并初始化
        //认证策略动态加载，通过spring容器获取所有实现（在加载时，每个对像需要克隆一份）或通过反射方式获取所有实现并初始化
        //添加新认证策略时，只需要实现对应接口或类，不需要修改公共代码
        strategyDefineServiceMap.put(sysId,strategyMap);
        //HashMap<AuthType, List<IStrategyDefineService>> result = new HashMap<>();
        if (StringUtils.isBlank(sysId)) {
            LOGGER.debug("sysId为空");
        }
        Long initTime = creStrategyInitTimeMap.get(sysId);
        if (Objects.isNull(initTime)) {
            initTime = 0L;
        }
        Long creStrategyInitTime = System.currentTimeMillis() - initTime;
        ReentrantLock lock = new ReentrantLock();
        if (creStrategyInitTime > CHANGE_TIME || strategyDefineServiceMap.get(sysId) == null) {
            lock.lock();
            List<StrategyDetail> strategies = strategyDetailMapper.getStrategyBySysId(sysId);
            //strategyMap.clear();
            //strategyMap.putIfAbsent(strategies);

            //既然要查询数据库来了，concurrentHashMap里的缓存肯定要失效，必须清空
            strategyMap.put(AuthType.BEFORE,new ArrayList<IStrategyDefineService>());
            strategyMap.put(AuthType.AFTER,new ArrayList<IStrategyDefineService>());
            strategyMap.put(AuthType.ERROR,new ArrayList<IStrategyDefineService>());
            strategyMap.put(AuthType.SUCCESS,new ArrayList<IStrategyDefineService>());
            strategyMap.put(AuthType.PASSWD,new ArrayList<IStrategyDefineService>());

            for (StrategyDetail strategy : strategies) {
                String strategyName = strategy.getCrelName();
                IStrategyDefineService newStrategy = null;
                switch (strategyName){
                    case "first_login":
                        newStrategy = new FirstLoginStrategyDefineServiceImpl(strategy);
                        //newStrategy = FirstLoginStrategyDefineServiceImpl.
                        System.out.println(newStrategy);break;
                    case "not_offer_ip":
                        newStrategy = new NotOffenIpStrategyDefineServiceImpl(strategy);break;
                }
                if (strategy.getAuthType().equals("1"/*AuthType.BEFORE*/)) {
                    System.out.println(newStrategy);
                    List<IStrategyDefineService> list = strategyMap.get(AuthType.BEFORE);
                    list.add(newStrategy);
                    strategyMap.put(AuthType.BEFORE, list);
                    System.out.println(strategyMap);
                } else if (strategy.getAuthType().equals(AuthType.SUCCESS)) {
                    List<IStrategyDefineService> list = strategyMap.get(AuthType.SUCCESS);
                    list.add(newStrategy);
                    strategyMap.put(AuthType.SUCCESS, list);
                } else if (strategy.getAuthType().equals(AuthType.ERROR)) {
                    List<IStrategyDefineService> list = strategyMap.get(AuthType.ERROR);
                    list.add(newStrategy);
                    strategyMap.put(AuthType.ERROR, list);
                } else if (strategy.getAuthType().equals(AuthType.AFTER)) {
                    List<IStrategyDefineService> list = strategyMap.get(AuthType.AFTER);
                    list.add(newStrategy);
                    strategyMap.put(AuthType.AFTER, list);
                } else if (strategy.getAuthType().equals(AuthType.PASSWD)) {
                    List<IStrategyDefineService> list = strategyMap.get(AuthType.PASSWD);
                    list.add(newStrategy);
                    strategyMap.put(AuthType.PASSWD, list);
                }
            }
            lock.unlock();
        }
        creStrategyInitTimeMap.put(sysId,System.currentTimeMillis());
        strategyDefineServiceMap.put(sysId,strategyMap);
        //result.putAll(strategyMap);
        return strategyDefineServiceMap.get(sysId);
    }

    private void loadAll(List<IStrategyDefineService> strategyDetails) {
        String[] icsNames = SpringContextUtil.getBeanNamesForType(IStrategyDefineService.class);
        for (String name : icsNames) {
            IStrategyDefineService strategy = SpringContextUtil.getBean(name);



            /*if (strategy.getAuthType().equals(AuthType.BEFORE)) {
                List<IStrategyDefineService> list = strategyMap.get(AuthType.BEFORE);
                list.add(strategy);
                strategyMap.put(AuthType.BEFORE, list);
            } else if (strategy.getAuthType().equals(AuthType.SUCCESS)) {
                List<IStrategyDefineService> list = strategyMap.get(AuthType.SUCCESS);
                list.add(strategy);
                strategyMap.put(AuthType.SUCCESS, list);
            } else if (strategy.getAuthType().equals(AuthType.ERROR)) {
                List<IStrategyDefineService> list = strategyMap.get(AuthType.ERROR);
                list.add(strategy);
                strategyMap.put(AuthType.ERROR, list);
            } else if (strategy.getAuthType().equals(AuthType.AFTER)) {
                List<IStrategyDefineService> list = strategyMap.get(AuthType.AFTER);
                list.add(strategy);
                strategyMap.put(AuthType.AFTER, list);
            } else if (strategy.getAuthType().equals(AuthType.PASSWD)) {
                List<IStrategyDefineService> list = strategyMap.get(AuthType.PASSWD);
                list.add(strategy);
                strategyMap.put(AuthType.PASSWD, list);
            }*/
        }
    }
}
