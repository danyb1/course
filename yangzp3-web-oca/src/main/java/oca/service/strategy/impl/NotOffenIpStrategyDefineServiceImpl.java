package oca.service.strategy.impl;


import oca.constants.BaseConstants;
import oca.domain.StrategyDetail;
import oca.domain.UserDetail;
import oca.dto.StrategyMessageDto;
import oca.dto.UserLoginReqDto;
import oca.enums.AuthType;


/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/29 15:59
 */
public class NotOffenIpStrategyDefineServiceImpl extends AbstractDefineServiceImpl {
    @Override
    public StrategyMessageDto validate(UserLoginReqDto reqDto, UserDetail userDetail) {
        return new StrategyMessageDto();
    }

    @Override
    public AuthType getAuthType() {
        return AuthType.BEFORE;
    }

    @Override
    public String getStrategyName() {
        //return
        return BaseConstants.LOGIN_IP_RULE;
    }
    /*@Override
    public void initStrategy(StrategyDetail stra) {
        super.initStrategy(stra);
        if(!BaseConstants.ENABLE_FLAG_TRUE.equals(stra.getCrelDetail())){
            this.setEnable(false);
        }
    }*/

    public NotOffenIpStrategyDefineServiceImpl(StrategyDetail strategyDetail){
        this.initStrategy(strategyDetail);
    }
    public NotOffenIpStrategyDefineServiceImpl(){}
}
