package oca.service.strategy.impl;


import oca.constants.BaseConstants;
import oca.domain.StrategyDetail;
import oca.domain.UserDetail;
import oca.dto.StrategyMessageDto;
import oca.dto.UserLoginReqDto;
import oca.enums.AuthType;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class FirstLoginStrategyDefineServiceImpl extends AbstractDefineServiceImpl {
    @Override
    public StrategyMessageDto validate(UserLoginReqDto reqDto, UserDetail userDetail) {
        StrategyMessageDto messageDto =  new StrategyMessageDto();;


        if(isEnable()&&StringUtils.isBlank(userDetail.getLastLoginTime())){
            //this.setActionType("3");

            messageDto.setAction(BaseConstants.USER_STATE_WARNING);
            messageDto.setStrategyName(getStrategyName());
            messageDto.setMessage("首次登录，触发警告");
            //messageDto.setLevel();
        }else{

            messageDto.setAction(BaseConstants.USER_STATE_NORMAL);
            messageDto.setStrategyName(getStrategyName());
            messageDto.setMessage("首次登录,应用状态正常");
            //messageDto.setAction(getActionType());
        }
        return messageDto;
    }

    @Override
    public AuthType getAuthType() {
        return AuthType.BEFORE;
    }

    @Override
    public String getStrategyName() {
        //return BaseConstants.LOGIN_FIRST_RULE;
        return creDetail;
    }

   /* @Override
    public void initStrategy(StrategyDetail stra) {
        super.initStrategy(stra);
        if(!BaseConstants.ENABLE_FLAG_TRUE.equals(stra.getCrelDetail())){ //策略的crelDetail属性为策略是否启用
            this.setEnable(false);
        }
    }*/

    public FirstLoginStrategyDefineServiceImpl(StrategyDetail strategyDetail){
        //super();
        //this.initStrategy(strategyDetail);
        /*this.setEnable(BaseConstants.ENABLE_FLAG_TRUE.equals(strategyDetail.getEnableFlag()));
        this.setActionType(strategyDetail.getActionType());
        this.setCreId(strategyDetail.getCrelId());
        this.setCreDetail(strategyDetail.getCrelDetail());*/
        this.actionType = strategyDetail.getActionType();
        this.creId = strategyDetail.getCrelId();
        this.creDetail = strategyDetail.getCrelDetail();
        this.enable = BaseConstants.ENABLE_FLAG_TRUE.equals(strategyDetail.getEnableFlag());
    }
    public FirstLoginStrategyDefineServiceImpl(){}
}
