package oca.service.strategy;



import oca.domain.StrategyDetail;
import oca.enums.AuthType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public interface IStrategyInitService {
    /**
     * 根据系统ID初始化认证策略
     * @param sysId
     * @return
     */
    public Map<AuthType, List<IStrategyDefineService>> initStrategy(String sysId);
}
