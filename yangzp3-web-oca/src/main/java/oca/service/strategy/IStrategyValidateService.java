package oca.service.strategy;



import oca.domain.UserDetail;
import oca.dto.StrategyMessageDto;
import oca.dto.UserLoginReqDto;

import java.util.List;

public interface IStrategyValidateService {

    /**
     * 交易前策略执行
     * @param reqDto
     * @param userDetail
     * @return
     */
    List<StrategyMessageDto> beforeAuth(UserLoginReqDto reqDto, UserDetail userDetail);

    /**
     * 交易后策略执行
     * @param reqDto
     * @param userDetail
     * @return
     */
    List<StrategyMessageDto> afterAuth(UserLoginReqDto reqDto, UserDetail userDetail);

    /**
     * 安全认证失败执行策略
     * @param reqDto
     * @param userDetail
     * @return
     */
    List<StrategyMessageDto> errorAuth(UserLoginReqDto reqDto, UserDetail userDetail);

    /**
     * 安全认证成功执行策略
     * @param reqDto
     * @param userDetail
     * @return
     */
    List<StrategyMessageDto> successAuth(UserLoginReqDto reqDto, UserDetail userDetail);

    /**
     * 密码强度验证策略
     * @param reqDto
     * @param userDetail
     * @return
     */
    List<StrategyMessageDto> passwdAuth(UserLoginReqDto reqDto, UserDetail userDetail);

}
