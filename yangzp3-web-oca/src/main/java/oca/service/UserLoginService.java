package oca.service;


import oca.constants.BaseConstants;
import oca.domain.UserDetail;
import oca.dto.StrategyMessageDto;
import oca.dto.UserLoginRepDto;
import oca.dto.UserLoginReqDto;
import oca.exception.BusinessException;
import oca.exception.BusinessExceptionCode;
import oca.mapper.UserDetailMapper;
import oca.service.auth.IUserAuthService;
import oca.service.strategy.IStrategyValidateService;
import oca.util.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserLoginService {

    @Autowired
    private IStrategyValidateService strategyValidateService;
    @Autowired
    private UserDetailMapper userMapper;

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    /**
     * oca执行安全验证（UAA执行安全验证时，把此方法内容分两个接口执行）
     * @param userDto
     * @return
     */
    public UserLoginRepDto login(UserLoginReqDto userDto){

        //根据认证类型，获取授权实现
        IUserAuthService authService = SpringContextUtil.getBean(userDto.getLoginType()+ BaseConstants.USER_AUTH_SERVICE_NAME);
        String loginCode = authService.getLoginCode(userDto);
        if(StringUtils.isEmpty(loginCode)){
            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
        }else{
            userDto.setLoginCode(loginCode);
        }
        //登录用户状态、机构状态判断
        UserDetail userDetail = this.checkUserState(loginCode);
        this.checkUserOrgState(loginCode);

        //前置认证策略验证
        UserLoginRepDto userLoginRepDto = this.beforeAuth(userDto, userDetail);
        if(!userLoginRepDto.getResult()){
            return userLoginRepDto;
        }

        //认证信息验证
        if(!authService.auth(userDto)) {
            strategyValidateService.errorAuth(userDto, userDetail);
            authService.authFailed(userDto);
        }else{
            strategyValidateService.successAuth(userDto, userDetail);
            userLoginRepDto.setResult(true);
        }
        strategyValidateService.afterAuth(userDto, userDetail);
        return userLoginRepDto;
    }


    private UserLoginRepDto beforeAuth(UserLoginReqDto userDto, UserDetail userDetail){
        UserLoginRepDto userLoginRepDto = new UserLoginRepDto();
        List<StrategyMessageDto> authDetails = strategyValidateService.beforeAuth(userDto, userDetail);
        if(authDetails != null){
            List<StrategyMessageDto> errorAuthDetails = authDetails.stream().filter((a->(BaseConstants.USER_STATE_WARNING.equals(a.getAction())))).collect(Collectors.toList());
            if(!errorAuthDetails.isEmpty()){ //包含非警告类策略时
                userLoginRepDto.setCode("0001");
                userLoginRepDto.setResult(false);
                userLoginRepDto.setMessage("执行认证策略失败");
                userLoginRepDto.setStrategyMessage(errorAuthDetails);
            }else{
                userLoginRepDto.setCode("0000");
                userLoginRepDto.setResult(true);
                userLoginRepDto.setMessage("执行认证策略成功");
                userLoginRepDto.setStrategyMessage(authDetails);
            }
        }
        return userLoginRepDto;
    }

    private void checkUserOrgState(String loginCode) {
    }


    private UserDetail checkUserState(String loginCode) {
        //return new UserDetail();
        //return userDetailMapper.getUserByLoginCode(loginCode);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserDetailMapper mapper = sqlSession.getMapper(UserDetailMapper.class);
        UserDetail userDetail = mapper.getUserByLoginCode(loginCode);
        return userDetail;
    }



    /*public void testUserExist() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserDetailMapper mapper = sqlSession.getMapper(UserDetailMapper.class);
        UserDetail yzp = mapper.getUserByLoginCode("yzp");
        System.out.println(yzp);
    }*/
}
