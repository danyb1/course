package oca.controller;


import oca.dto.ResponseDto;
import oca.dto.UserLoginRepDto;
import oca.dto.UserLoginReqDto;
import oca.exception.BusinessException;
import oca.exception.BusinessExceptionCode;
import oca.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/user")
public class UserLoginController {

    @Autowired
    private UserLoginService userLoginService;


    private  BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @PostMapping("/login")
    public ResponseDto login(@RequestBody UserLoginReqDto userLoginReqDto){
        ResponseDto responseDto = new ResponseDto();
        try {
            UserLoginRepDto userLoginRepDto = userLoginService.login(userLoginReqDto);
            responseDto.setData(userLoginRepDto);
        }catch (BusinessException e){
            UserLoginRepDto userLoginRepDto = new UserLoginRepDto();
            userLoginRepDto.setResult(false);
            userLoginRepDto.setCode(e.getCode());
            userLoginRepDto.setMessage(e.getMessage());

            responseDto.setData(userLoginRepDto);
        }catch (Exception all){
            responseDto.setCode(BusinessExceptionCode.LOGIN_ERROR.getCode());
            responseDto.setMessage(BusinessExceptionCode.LOGIN_ERROR.getDesc());
        }
        return responseDto;
    }


    @PostMapping("/login_")
    public ResponseDto login(){
        ResponseDto<Object> responseDto = new ResponseDto<>();
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        String password = request.getParameter("password");
        String userCode = request.getParameter("userCode");

        if (userCode.equals("uaa")&&password.equals("123456")){
            responseDto.setCode("0000");
            responseDto.setMessage("认证成功，密码校验成功");
            responseDto.setData(bCryptPasswordEncoder.encode(password));
        }else{
            responseDto.setCode("0001");
            responseDto.setMessage("认证失败或密码校验失败");
            responseDto.setData("没有数据");
        }
        return responseDto;
    }

    @PostMapping("/loadUser")
    public ResponseDto loadUser(){
        return null;
    }

    @PostMapping("/updateUser")
    public ResponseDto updateUser(){
        return null;
    }
}
