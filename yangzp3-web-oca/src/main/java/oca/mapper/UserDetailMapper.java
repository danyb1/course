package oca.mapper;

import oca.domain.UserDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/28 14:32
 */
@Mapper
public interface UserDetailMapper {
    @Select("select * from user_detail where loginCode=#{loginCode} ")
    public UserDetail getUserByLoginCode(String loginCode);
}






















