package oca.mapper;

import oca.domain.StrategyDetail;
import oca.service.strategy.IStrategyDefineService;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/29 11:14
 */
@Mapper
public interface StrategyDetailMapper {
    @Select("select * from strategy_detail where sys_id = #{sysId}")
    public List<StrategyDetail> getStrategyBySysId(String sysId);
}
