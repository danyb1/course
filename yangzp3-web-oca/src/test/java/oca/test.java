package oca;


import oca.domain.StrategyDetail;
import oca.domain.UserDetail;
import oca.mapper.StrategyDetailMapper;
import oca.mapper.UserDetailMapper;
import oca.util.SpringContextUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/29 10:06
 */
@SpringBootTest
@RunWith(SpringRunner.class)
//@Import({UserDetailMapper.class})
public class test {
    @Autowired
    private UserDetailMapper userMapper;
    @Autowired
    private StrategyDetailMapper strategyDetailMapper;

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Test
    public void testUserExist() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserDetailMapper mapper = sqlSession.getMapper(UserDetailMapper.class);
        UserDetail yzp = mapper.getUserByLoginCode("yzp");
        System.out.println(yzp);
    }

    @Test
    public void testStrategyExist() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        StrategyDetailMapper mapper = sqlSession.getMapper(StrategyDetailMapper.class);
        List<StrategyDetail> strategy = (List<StrategyDetail>) mapper.getStrategyBySysId("1");
        strategy.stream().forEach(System.out::println);
    }
}
