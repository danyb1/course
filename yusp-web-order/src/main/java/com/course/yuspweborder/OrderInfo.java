/**
 * 
 */
package com.course.yuspweborder;

import lombok.Data;

/**
 * @author jojo
 *
 */
@Data
public class OrderInfo {
	
	private Long id;
	
	private Long productId;
	
}
