/**
 * 
 */
package com.course.yuspweborder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author jojo
 *
 */
@RestController
@RequestMapping("/orders")
@Slf4j
public class OrderController {
	
//	private RestTemplate restTemplate = new RestTemplate();
	
	@PostMapping
	public OrderInfo create(@RequestBody OrderInfo info, @RequestHeader String username) {
		log.info("user is " + username);
//		PriceInfo price = restTemplate.getForObject("http://localhost:9060/prices/"+info.getProductId(), PriceInfo.class);
//		log.info("price is "+price.getPrice());
		return info;
	}
	
	@GetMapping("/{id}")
	public ResponseDto<OrderInfo> getInfo(@PathVariable Long id) {
//		log.info("user is " + username);
		log.info("orderId is " + id);
		OrderInfo info = new OrderInfo();
		info.setId(id);
		info.setProductId(id * 5);
		ResponseDto<OrderInfo> responseDto = new ResponseDto<>();
		responseDto.setContent(info);
		return responseDto;
	}

}
