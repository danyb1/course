package demo.course.order.controller;

import demo.course.order.dto.OrderInfo;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders")
public class OrderInfoController {

    /**
     * 在Oauth2WebSecurityConfig的配置中，资源服务器拦截到token，就会发请求到认证服务器获取用户的信息，
     *
     * @param orderInfo
     * @return
     * @AuthenticationPrincipal注解可以注入获取到的用户信息
     */
    @PostMapping
    // public OrderInfo create(@RequestBody OrderInfo orderInfo, @AuthenticationPrincipal String username) {
    //public OrderInfo create(@RequestBody OrderInfo orderInfo, @AuthenticationPrincipal User user) {
    public OrderInfo create(@RequestBody OrderInfo orderInfo) {

        return orderInfo;
    }

    @GetMapping("/{id}")
    public OrderInfo getById(@PathVariable String id) {
        //public OrderInfo getById(@PathVariable String id, @AuthenticationPrincipal User user) {
        // public OrderInfo getById(@PathVariable String id, @AuthenticationPrincipal(expression="#this.id") String userId) { // 使用表达式获取用户信息的某个属性
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setName("test order");
        return orderInfo;
    }

}
