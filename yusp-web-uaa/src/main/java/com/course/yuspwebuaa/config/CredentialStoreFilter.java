package com.course.yuspwebuaa.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

@Slf4j
@Component
public class CredentialStoreFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request=(HttpServletRequest)servletRequest;
        String username= Optional.ofNullable(request.getHeader("username")).orElseGet(()->(String)request.getSession().getAttribute("username"));
        Optional.ofNullable(username).ifPresent((value)->request.getSession().setAttribute("username",value));

        log.info("用户名:"+username);
        chain.doFilter(servletRequest,servletResponse);
    }
}
