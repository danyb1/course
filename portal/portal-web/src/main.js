import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
// 导入filter.js
import filter from './filter/filter'

Vue.config.productionTip = false
Vue.prototype.$ajax = axios

// 解决每次ajax请求，对应的sessionId不一致的问题
axios.defaults.withCredentials = true;

/**
 * axios拦截器
 */
// axios.interceptors.request.use(function (config) {
//   console.log("请求：", config);
//   let token = Tool.getLoginUser().token;
//   if (Tool.isNotEmpty(token)) {
//     config.headers.token = token;
//     console.log("请求headers增加token:", token);
//   }
//   return config;
// }, error => {});
// axios.interceptors.response.use(function (response) {
//   console.log("返回结果：", response);
//   return response;
// }, error => {});

// 增加全局过滤器
Object.keys(filter).forEach(key => {
  Vue.filter(key, filter[key])
});

function getCookie(objName) {//获取指定名称的cookie的值
  var arrStr = document.cookie.split("; ");
  for (var i = 0; i < arrStr.length; i++) {
    var temp = arrStr[i].split("=");
    if (temp[0] == objName) return unescape(temp[1]);  //解码
  }
  return "";
}

// 路由登录拦截
router.beforeEach((to, from, next) => {
  // 要不要对meta.loginRequire属性做监控拦截
  //console.log(JSON.stringify(from)+"---"+JSON.stringify(to))
  if (to.matched.some(function (item) {
    return item.meta.loginRequire
  })) {
    let cookie=getCookie("access_info")

    console.log("cookie:"+cookie);
    let access=decodeURI(cookie);
    console.log("access:"+access);
    if (!access) {
      next('/loginPage');
    } else {
      next();
    }
  } else {
    next();
  }
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');

console.log("环境：", process.env.VUE_APP_SERVER);


