import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/login.vue'
import Index from './views/index.vue'
import Hello from './views/hello.vue'
import Admin from './views/admin.vue'
import Welcome from './views/admin/welcome.vue'

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [{
        path: '*',
        redirect: '/',
    },{
        path: '/',
        name:'index',
        component: Index
    },{
        path: '/loginPage',
        name:'loginPage',
        component: Login
    },{
        path: '/hello',
        name:'hello',
        component: Hello,
        meta:{
            loginRequire:true
        }
    },{
        path: '/admin',
        name: 'admin',
        component: Admin,
        meta: {
            loginRequire: true
        },
        children: [{
            path: '/welcome',
            name: 'welcome',
            component: Welcome
        }]
    }]
})