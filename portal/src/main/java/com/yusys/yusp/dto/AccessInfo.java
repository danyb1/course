package com.yusys.yusp.dto;

import lombok.Data;

import java.util.ArrayList;

/**
 * 保存在redis的用户权限信息
 * 描述了用户可以访问的微服务和接口资源
 */
@Data
public class AccessInfo {

    private ArrayList<String> roles;
}
