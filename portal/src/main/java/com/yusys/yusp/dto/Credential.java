package com.yusys.yusp.dto;

import lombok.Data;

@Data
public class Credential {

    String username;
    String password;
}
