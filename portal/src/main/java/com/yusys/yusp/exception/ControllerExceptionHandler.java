package com.yusys.yusp.exception;

import com.yusys.yusp.dto.BusinessExceptionCode;
import com.yusys.yusp.dto.ResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;

@RestControllerAdvice
public class ControllerExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ControllerExceptionHandler.class);


    @ExceptionHandler(value = HttpClientErrorException.class)
    public ResponseDto businessExceptionHandler(HttpClientErrorException e) {
        ResponseDto responseDto = new ResponseDto();
        responseDto.setSuccess(false);
        LOG.error("业务异常：{}", e.getMessage());
        responseDto.setMessage(BusinessExceptionCode.LOGIN_USER_ERROR.getDesc());
        return responseDto;
    }
}
