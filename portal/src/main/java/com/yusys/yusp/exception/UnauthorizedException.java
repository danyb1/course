package com.yusys.yusp.exception;

import com.yusys.yusp.config.BffConstant;
import lombok.Data;

import java.util.Optional;

@Data
public class UnauthorizedException extends Exception {

    private String state;
    public UnauthorizedException(){
        this(BffConstant.MAIN_PAGE_ROUTE_PATH);
    }

    public UnauthorizedException(String state){
        super("You are not logged in yet,you will redirect to "+ Optional.ofNullable(state).orElse(BffConstant.MAIN_PAGE_ROUTE_PATH));
        this.state=Optional.ofNullable(state).orElse(BffConstant.MAIN_PAGE_ROUTE_PATH);
    }


}
