package com.yusys.yusp.config;

public class BffConstant {

    public static final String TOKEN_INFO_SESSION_KEY = "tokenInfo";
    public static final String ACCESS_INFO_COOKIE_KEY = "access_info";

    public static final String MAIN_PAGE_ROUTE_PATH = "/hello";

}
