package com.yusys.yusp.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.yusys.yusp.filter.*;
import com.yusys.yusp.service.SessionTokenService;
import com.yusys.yusp.service.TokenService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.text.SimpleDateFormat;

@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

//    @Bean
//    //@ConditionalOnProperty(prefix = "application.auth.portal",name = "token-info-store",havingValue = "session",matchIfMissing=true)
//    public AuthenticationFilter authenticationFilter(){
//        return new AuthenticationFilter();
//    }
//
//    @Bean
//    public AccessInfoServiceFilter accessInfoServiceFilter(){
//        return new AccessInfoServiceFilter();
//    }

    @Bean
    public  static TokenService tokenService(){
        return new SessionTokenService();
    }

    @Bean
    //@LoadBalanced
    public static RestTemplate restTemplate() {
        return new RestTemplate();
    }


    @Bean
    public static ObjectMapper objectMapper(){
        ObjectMapper objectMapper=new ObjectMapper();
        //序列化的时候序列对象的所有属性
        objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);

        //反序列化的时候如果多了其他属性,不抛出异常
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        //如果是空对象的时候,不抛异常
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        //取消时间的转化格式,默认是时间戳,可以取消,同时需要设置要表现的时间格式
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        return objectMapper;
    }


    @Configuration
    public static class AccessInfoServiceFilterConfiguration {

        private final BffProperties bffProperties;

        public AccessInfoServiceFilterConfiguration(BffProperties bffProperties) {
            this.bffProperties = bffProperties;
        }

        @Bean
        public AccessInfoHandler accessInfoHandler(){
            return new AccessInfoHandler(objectMapper());
        }

        @Bean
        public AccessInfoServiceFilter accessInfoServiceFilter() {
            return new AccessInfoServiceFilter(bffProperties,accessInfoHandler());
        }
    }


    @Configuration
    public static class AuthenticationFilterConfiguration {

        private final BffProperties bffProperties;

        public AuthenticationFilterConfiguration(BffProperties bffProperties) {
            this.bffProperties = bffProperties;
        }

        @Bean
        public AuthenticationHandler authenticationHandler(){
            return new AuthenticationHandler(bffProperties,restTemplate(),tokenService());
        }

        @Bean
        public AuthenticationFilter authenticationFilter() {
            return new AuthenticationFilter(authenticationHandler(),bffProperties);
        }
    }




    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
        super.addResourceHandlers(registry);
    }

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthenticationInterceptor()).addPathPatterns("/me");
        super.addInterceptors(registry);
    }


}
