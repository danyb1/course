package com.yusys.yusp.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

@Data
@ConfigurationProperties(prefix = "application.auth.portal")
public class BffProperties {

    public String TOKEN_URL = "/oauth/token";

    public String AUTHORIZE_URL = "/oauth/authorize";

    private String tokenInfoStore="session";

    private List<String> skipUrls= Arrays.asList("/portal/login","/portal/logout","/portal/callback");

    private String gateway="localhost:9070";

    private String clientId="portal";

    private String secret="123456";

    private String uaa="/uaa";


    public  String getRedirectURL() throws UnsupportedEncodingException {
        //return "/api"+uaa+AUTHORIZE_URL+"?response_type=code&&client_id="+clientId+"&&redirect_uri="+ URLEncoder.encode("http://localhost:8855/callback","UTF-8")+"&&state=login";
        return "http://auth.imooc.com:9090"+AUTHORIZE_URL+"?response_type=code&&client_id="+clientId+"&&redirect_uri="+ URLEncoder.encode("http://localhost:8855/callback","UTF-8")+"&&state=";
    }

    public String getTokenURL(){
        return gateway+uaa+TOKEN_URL;
    }

}
