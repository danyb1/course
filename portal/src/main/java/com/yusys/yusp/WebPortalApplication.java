package com.yusys.yusp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
@EnableZuulProxy
@CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
public class WebPortalApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebPortalApplication.class,args);
    }
}
