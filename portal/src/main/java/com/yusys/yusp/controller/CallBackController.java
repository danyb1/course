package com.yusys.yusp.controller;

import com.yusys.yusp.config.BffProperties;
import com.yusys.yusp.dto.TokenInfo;
import com.yusys.yusp.exception.UnauthorizedException;
import com.yusys.yusp.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Slf4j
@RestController
@CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
@RequestMapping
public class CallBackController {

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private BffProperties bffProperties;

    @Autowired
    private TokenService tokenService;



    /**
     * 登录时的回调接口，返回用户sessionInfo
     * @param code
     * @param state
     * @param request
     * @return
     */
    @CrossOrigin
    @GetMapping("/callback")
    public void callback(@RequestParam("code") String code,
                                 @RequestParam("state") String state,
                                 HttpServletRequest request, HttpServletResponse response) throws UnauthorizedException, IOException {
        //准备参数
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        httpHeaders.setBasicAuth(bffProperties.getClientId(), bffProperties.getSecret());

        MultiValueMap<String,String> param = new LinkedMultiValueMap<>();

        HttpEntity<MultiValueMap<String,String>> httpEntity = new HttpEntity<>(param,httpHeaders);

        ResponseEntity<TokenInfo> res =null;
        try {
            //换取tokenInfo
            res = restTemplate.postForEntity(bffProperties.getTokenURL()+"?code="+code+"&&grant_type=authorization_code&&redirect_uri=http://localhost:8855/callback", httpEntity, TokenInfo.class);
        }catch (Exception e) {
            log.warn("通过授权码获取token失败:{}",code);
            return ;
        }

        //本地保存
        Optional<TokenInfo> optional = Optional.ofNullable(res.getBody());
        optional.ifPresent(TokenInfo::init);
        optional.orElseThrow(() -> new UnauthorizedException(state));
        tokenService.saveTokenInfo(request,optional.get());
        response.sendRedirect(state);
    }

}
