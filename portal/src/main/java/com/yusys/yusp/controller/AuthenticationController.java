package com.yusys.yusp.controller;

import com.yusys.yusp.config.BffConstant;
import com.yusys.yusp.config.BffProperties;
import com.yusys.yusp.dto.Credential;
import com.yusys.yusp.dto.ResponseDto;
import com.yusys.yusp.dto.TokenInfo;
import com.yusys.yusp.filter.AuthenticationHandler;
import com.yusys.yusp.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@RestController
@Slf4j
public class AuthenticationController {

    @Autowired
    public TokenService tokenService;

    @Autowired
    private BffProperties bffProperties;

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private AuthenticationHandler authenticationHandler;



    @GetMapping("/me")
    public void me(HttpServletResponse response) throws IOException {
        response.sendRedirect(BffConstant.MAIN_PAGE_ROUTE_PATH);
    }

    @PostMapping("/login")
    public ResponseDto<Boolean> login(Credential credential, HttpServletRequest request )   {
        String oauthServiceUrl = bffProperties.getGateway() + "/uaa/oauth/token";
        //准备参数
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBasicAuth(bffProperties.getClientId(), bffProperties.getSecret());

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("username", credential.getUsername());
        params.add("password", credential.getPassword());
        params.add("grant_type", "password");
        params.add("scope", "read");

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
        //请求tokenInfo
        ResponseEntity<TokenInfo> tokenInfo = restTemplate.exchange(oauthServiceUrl, HttpMethod.POST, entity, TokenInfo.class);
        Optional<TokenInfo> optional = Optional.ofNullable(tokenInfo.getBody());
        optional.ifPresent(TokenInfo::init);
        //optional.orElseThrow(() -> new UnauthorizedException("hello"));//登录失败
        //本地保存
        request.getSession().setAttribute(BffConstant.TOKEN_INFO_SESSION_KEY, optional.get());
        return new ResponseDto();
    }


    @RequestMapping("/logout")
    public ResponseDto<Boolean> logout(HttpServletRequest request,HttpServletResponse response)  {
        ResponseDto res=new ResponseDto();
        String logoutUrl = bffProperties.getGateway() + "/uaa/logout";

        HttpHeaders headers = new HttpHeaders();
        try {
            headers.setBearerAuth( authenticationHandler.getAccessToken(request,response));
            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(headers);
            //全局登出
            ResponseEntity<Object> responseEntity =restTemplate.exchange(logoutUrl, HttpMethod.POST, entity, Object.class);
            //本地登出
            tokenService.clearTokenInfo(request);

        } catch (Exception e) {
            res.setSuccess(false);
            res.setMessage("token has already empired!");
        }
        return res;
    }

}
