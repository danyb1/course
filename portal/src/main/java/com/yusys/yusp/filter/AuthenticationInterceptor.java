package com.yusys.yusp.filter;

import com.yusys.yusp.util.SpringContextUtil;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthenticationInterceptor implements HandlerInterceptor {

    private AuthenticationHandler authenticationHandler = SpringContextUtil.getContext().getBean(AuthenticationHandler.class);


    /**
     * 请求本地api时触发
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String tokenValue = authenticationHandler.getAccessToken(request, response);
        if (StringUtils.isEmpty(tokenValue)) {
            return false;
        }
        return true;
    }
}
