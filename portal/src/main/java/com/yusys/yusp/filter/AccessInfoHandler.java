package com.yusys.yusp.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yusys.yusp.config.BffConstant;
import com.yusys.yusp.dto.AccessInfo;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;


public class AccessInfoHandler {

    private  final Base64.Encoder encoder = Base64.getEncoder();

    public AccessInfoHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private final ObjectMapper objectMapper;

    public void handle(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ArrayList<String> roles=new ArrayList<String>(Arrays.asList("a","b"));
        AccessInfo accessInfo=new AccessInfo();
        accessInfo.setRoles(roles);
        if(request.getRequestURI().equals("/logout")){
            //清空cookie
            Cookie[] cookies=request.getCookies();
            for(Cookie cookie:cookies) {
                //if(cookie.getName().equals(BffConstant.ACCESS_INFO_COOKIE_KEY)){
                    cookie.setMaxAge(0);
                    cookie.setPath("/");
                    response.addCookie(cookie);

                //}
            }
            response.sendRedirect("/");

        }else{
                Cookie accessTokenCookie = new Cookie(BffConstant.ACCESS_INFO_COOKIE_KEY, URLEncoder.encode(objectMapper.writeValueAsString(accessInfo), "UTF-8"));
                accessTokenCookie.setMaxAge(12 * 3600);//用户权限信息在浏览器停留半天
                accessTokenCookie.setDomain("localhost");
                accessTokenCookie.setPath("/");
                response.addCookie(accessTokenCookie);
            }


    }
}
