package com.yusys.yusp.filter;

import com.yusys.yusp.config.BffConstant;
import com.yusys.yusp.config.BffProperties;
import com.yusys.yusp.dto.TokenInfo;
import com.yusys.yusp.exception.UnauthorizedException;
import com.yusys.yusp.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;


@Slf4j
//@Component
public class AuthenticationHandler {
    protected BffProperties bffProperties;

    protected RestTemplate restTemplate;

    private final TokenService tokenService;


    public AuthenticationHandler(BffProperties bffProperties, RestTemplate restTemplate, TokenService tokenService) {
        this.bffProperties = bffProperties;
        this.restTemplate = restTemplate;
        this.tokenService = tokenService;
    }

    public String getAccessToken(HttpServletRequest request, HttpServletResponse response){
        String accessToken=null;
        try{
            accessToken=this.obtainAccessToken(request);
        }catch(UnauthorizedException e){
            try {
                //response.sendRedirect(bffProperties.getRedirectURL()+e.getState());
                response.sendRedirect("/loginPage");
            } catch (IOException ioException) {
                log.error("Unable to send a redirect to "+e.getState());
            }
        }
        return accessToken;
    }


    /**
     * 获取保存在本地session或cookie中的access_token字符串，包含刷新逻辑
     * @param request
     * @return
     * @throws UnauthorizedException 授权码模式下，未授权的请求统一跳转认证中心，重新认证
     */
    public String obtainAccessToken(HttpServletRequest request) throws UnauthorizedException {
        String accessToken = null;
        TokenInfo tokenInfo =  tokenService.getTokenInfo(request);
        String url = request.getRequestURL().toString();

        if (tokenInfo == null) {// 从未登录或调用了登出
            //重定向到登录页面
            throw new UnauthorizedException(request.getHeader("state"));
        } else {
            //登录过未登出
            accessToken = tokenInfo.getAccess_token();
            if (tokenInfo.isExpired()) {
                String oauthServiceUrl = bffProperties.getGateway() + "/token/oauth/token";

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
                headers.setBasicAuth(bffProperties.getClientId(), bffProperties.getSecret());

                MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
                params.add("grant_type", "refresh_token");
                params.add("refresh_token", tokenInfo.getRefresh_token());

                HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);

                ResponseEntity<TokenInfo> newToken = restTemplate.exchange(oauthServiceUrl, HttpMethod.POST, entity, TokenInfo.class);
                Optional<TokenInfo> optional = Optional.ofNullable(newToken.getBody());
                optional.ifPresent(TokenInfo::init);
                //todo 先传url，后面要改为从请求里获取当前页面的路由id，前端可能要加过滤器
                optional.orElseThrow(() -> new UnauthorizedException(url));//刷新token也过期，重新登录
                request.getSession().setAttribute(BffConstant.TOKEN_INFO_SESSION_KEY, optional.get());
                accessToken = newToken.getBody().getAccess_token();
            }

        }

        return accessToken;
    }
}
