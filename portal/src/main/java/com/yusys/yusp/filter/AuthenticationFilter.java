package com.yusys.yusp.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.yusys.yusp.config.BffProperties;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 验证登录,两种存取token信息的方式session和cookie
 */
//@Component
public class AuthenticationFilter extends ZuulFilter {

    private AuthenticationHandler authenticationHandler;

    protected BffProperties bffProperties;

    public AuthenticationFilter(AuthenticationHandler authenticationHandler, BffProperties bffProperties) {
        this.authenticationHandler = authenticationHandler;
        this.bffProperties = bffProperties;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        return bffProperties.getSkipUrls().stream().anyMatch(url -> url.equals(request.getRequestURI()));
    }

    /**
     * 路由转发时触发
     *
     * @return
     * @throws ZuulException
     */
    @Override
    public Object run() throws ZuulException {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        HttpServletResponse response = requestContext.getResponse();
        String tokenValue = authenticationHandler.getAccessToken(request, response);
        if (StringUtils.isEmpty(tokenValue)) {
            requestContext.setSendZuulResponse(false);
        } else {
            requestContext.addZuulRequestHeader("Authorization", "Bearer " + tokenValue);
        }
        return null;
    }



    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }


}
