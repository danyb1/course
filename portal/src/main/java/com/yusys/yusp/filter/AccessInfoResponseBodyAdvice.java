package com.yusys.yusp.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.io.IOException;

@ControllerAdvice
public class AccessInfoResponseBodyAdvice implements ResponseBodyAdvice<Object> {

    @Autowired
    private AccessInfoHandler accessInfoHandler;

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        ServletServerHttpRequest req=(ServletServerHttpRequest)request;
        ServletServerHttpResponse res=(ServletServerHttpResponse)response;
        try {
            accessInfoHandler.handle(req.getServletRequest(),res.getServletResponse());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return body;
    }
}
