package com.yusys.yusp.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.yusys.yusp.config.BffProperties;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
//@Component
public class AccessInfoServiceFilter extends ZuulFilter {

    protected BffProperties bffProperties;

    private AccessInfoHandler accessInfoHandler;

    public AccessInfoServiceFilter(BffProperties bffProperties, AccessInfoHandler accessInfoHandler) {
        this.bffProperties = bffProperties;
        this.accessInfoHandler = accessInfoHandler;
    }

    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        return bffProperties.getSkipUrls().stream().anyMatch(url -> url.equals(request.getRequestURI()));
    }

    @Override
    public Object run()   {
        //TODO 从session取tokenInfo，从tokenInfo取用户id，向oca发请求获取菜单控制点权限列表，写进cookie返回浏览器
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request=requestContext.getRequest();
        HttpServletResponse response = requestContext.getResponse();
        try {
            accessInfoHandler.handle(request,response);
        }  catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }








}
