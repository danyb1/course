package com.yusys.yusp.service;

import com.yusys.yusp.dto.TokenInfo;

import javax.servlet.http.HttpServletRequest;

public interface TokenService {

    void saveTokenInfo(HttpServletRequest request,TokenInfo tokenInfo);

    TokenInfo getTokenInfo(HttpServletRequest request);

    void clearTokenInfo(HttpServletRequest request);
}
