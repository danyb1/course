package com.yusys.yusp.service;


import com.yusys.yusp.config.BffConstant;
import com.yusys.yusp.dto.TokenInfo;

import javax.servlet.http.HttpServletRequest;

/**
 * 基于session存储的tokenInfo管理组件
 *
 */
//@Service
public class SessionTokenService implements TokenService {

    public void saveTokenInfo(HttpServletRequest request,TokenInfo tokenInfo){
        request.getSession().setAttribute(BffConstant.TOKEN_INFO_SESSION_KEY, tokenInfo);
    }


    public TokenInfo getTokenInfo(HttpServletRequest request){
        TokenInfo tokenInfo = (TokenInfo) request.getSession().getAttribute(BffConstant.TOKEN_INFO_SESSION_KEY);
        return tokenInfo;
    }

    @Override
    public void clearTokenInfo(HttpServletRequest request) {
        request.getSession().invalidate();
    }


}
