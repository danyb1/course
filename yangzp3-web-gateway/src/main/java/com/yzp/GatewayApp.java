package com.yzp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/18 18:49
 */
@Slf4j
@SpringBootApplication
@EnableZuulProxy
public class GatewayApp {
    public static void main(String[] args) {
        Environment env = SpringApplication.run(GatewayApp.class, args).getEnvironment();
        log.info("Gateway已启动");
    }
}
