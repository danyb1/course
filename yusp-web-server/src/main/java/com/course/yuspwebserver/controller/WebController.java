package com.course.yuspwebserver.controller;

import com.course.yuspwebserver.dto.Credentials;
import com.course.yuspwebserver.dto.ResponseDto;
import com.course.yuspwebserver.dto.TokenInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@Slf4j
public class WebController {

    private RestTemplate restTemplate = new RestTemplate();

    @GetMapping("/me")
    public TokenInfo me(HttpServletRequest request) {
        TokenInfo info = (TokenInfo)request.getSession().getAttribute("token");
        return info;
    }

    @GetMapping("/oauth/callback")
    public void callback(@RequestParam String code, String state, HttpServletRequest request, HttpServletResponse response) throws IOException {

        log.info("state is " + state);

        String oauthServiceUrl = "http://gateway.imooc.com:9070/token/oauth/token";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBasicAuth("admin", "123456");

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("code", code);
        params.add("grant_type", "authorization_code");
        params.add("redirect_uri", "http://admin.imooc.com:8080/yuspwebserver/oauth/callback");

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);

        ResponseEntity<TokenInfo> token = restTemplate.exchange(oauthServiceUrl, HttpMethod.POST, entity, TokenInfo.class);
		request.getSession().setAttribute("token", token.getBody().init());

//        Cookie accessTokenCookie = new Cookie("imooc_access_token", token.getBody().getAccess_token());
//        accessTokenCookie.setMaxAge(token.getBody().getExpires_in().intValue());
//        accessTokenCookie.setDomain("imooc.com");
//        accessTokenCookie.setPath("/");
//        response.addCookie(accessTokenCookie);

//        Cookie refreshTokenCookie = new Cookie("imooc_refresh_token", token.getBody().getRefresh_token());
//        refreshTokenCookie.setMaxAge(2592000);
//        refreshTokenCookie.setDomain("imooc.com");
//        refreshTokenCookie.setPath("/");
//        response.addCookie(refreshTokenCookie);

        response.sendRedirect("http://admin.imooc.com:8080/login");
    }

    @PostMapping("/login")
    public ResponseDto<TokenInfo> login(@RequestBody Credentials credentials, HttpServletRequest request) {

        String zuulGatewayUrl = "http://localhost:9070/token/oauth/token";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBasicAuth("test", "123456");

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("username", credentials.getUsername());
        params.add("password", credentials.getPassword());
        params.add("grant_type", "password");
        params.add("scope", "read write");

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);

        ResponseEntity<TokenInfo> response = restTemplate.exchange(zuulGatewayUrl, HttpMethod.POST, entity, TokenInfo.class);
        TokenInfo tokenInfo = new TokenInfo();
        tokenInfo = response.getBody();
        log.info("token info :" + tokenInfo.toString());
        request.getSession().setAttribute("token", tokenInfo);

        ResponseDto<TokenInfo> responseDto = new ResponseDto<>();
        responseDto.setContent(tokenInfo);
        return responseDto;

    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().invalidate();
    }

}
