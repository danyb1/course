package com.course.yuspwebserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.core.env.Environment;

@SpringBootApplication(scanBasePackages = "com.course")
@EnableZuulProxy
public class YuspWebServerApplication {

    private static final Logger LOG = LoggerFactory.getLogger(YuspWebServerApplication.class);

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(YuspWebServerApplication.class);
        Environment env = app.run(args).getEnvironment();
        LOG.info("启动成功！！");
        LOG.info("YuspwebServer地址: \thttp://127.0.0.1:{}", env.getProperty("server.port"));
    }

}
