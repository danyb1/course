/**
 * 
 */
package com.course.yuspwebserver.dto;

import lombok.Data;

/**
 * @author jojo
 *
 */
@Data
public class Credentials {

	private String username;
	
	private String password;
	
}
