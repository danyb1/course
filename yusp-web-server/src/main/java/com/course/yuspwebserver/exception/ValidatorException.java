package com.course.yuspwebserver.exception;

public class ValidatorException extends RuntimeException {

    public ValidatorException(String message) {
        super(message);
    }
}
