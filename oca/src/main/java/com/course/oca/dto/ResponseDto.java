package com.course.oca.dto;

public class ResponseDto<T> {
    /**
     * 返回码
     */
    private String code = "0";

    /**
     * 返回信息
     */
    private String message = "success";

    /**
     * 返回泛型数据，自定义类型
     */
    private T data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
