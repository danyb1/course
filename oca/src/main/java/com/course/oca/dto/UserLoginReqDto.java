package com.course.oca.dto;

import java.util.Map;

public class UserLoginReqDto {
    /**
     * 系统编号
     */
    private String sysId;
    /**
     * 登录类型（密码：password）
     */
    private String loginType;
    /**
     * 登录号
     */
    private String loginCode;
    /**
     * 授权信息
     */
    private String authCode;
    /**
     * 扩展信息
     */
    private Map<String, Object> extendsInfo;

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public Map<String, Object> getExtendsInfo() {
        return extendsInfo;
    }

    public void setExtendsInfo(Map<String, Object> extendsInfo) {
        this.extendsInfo = extendsInfo;
    }
}
