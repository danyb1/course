package com.course.oca.service.strategy;

import com.course.oca.enums.AuthType;

import java.util.List;
import java.util.Map;

public interface IStrategyInitService {
    /**
     * 根据系统ID初始化认证策略
     * @param sysId
     * @return
     */
    public Map<AuthType, List<IStrategyDefineService>> initStrategy(String sysId);
}
