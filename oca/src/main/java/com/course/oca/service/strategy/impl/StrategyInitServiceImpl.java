package com.course.oca.service.strategy.impl;

import com.course.oca.domain.StrategyDetail;
import com.course.oca.enums.AuthType;
import com.course.oca.service.strategy.IStrategyDefineService;
import com.course.oca.service.strategy.IStrategyInitService;
import com.course.oca.util.SpringContextUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class StrategyInitServiceImpl implements IStrategyInitService {
    private ConcurrentHashMap<String, Map<AuthType, List<IStrategyDefineService>>> strategyDefineServiceMap = new ConcurrentHashMap<>();
    @Override
    public Map<AuthType, List<IStrategyDefineService>> initStrategy(String sysId) {
        //根据系统ID初始化认证策略
        //认证策略设置一定缓存时间
        //认证策略加载时，根据认证策略名称与实现类getStrategyName返回值匹配并初始化
        //认证策略动态加载，通过spring容器获取所有实现（在加载时，每个对像需要克隆一份）或通过反射方式获取所有实现并初始化
        //添加新认证策略时，只需要实现对应接口或类，不需要修改公共代码

        return null;
    }

    private void loadAll(List<StrategyDetail> strategyDetails){
        String[] icsNames = SpringContextUtil.getBeanNamesForType(IStrategyDefineService.class);
        for (String name : icsNames) {
            IStrategyDefineService creStrategy = SpringContextUtil.getBean(name);
        }
    }
}
