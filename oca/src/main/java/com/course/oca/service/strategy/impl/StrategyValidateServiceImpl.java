package com.course.oca.service.strategy.impl;

import com.course.oca.constants.BaseConstants;
import com.course.oca.domain.UserDetail;
import com.course.oca.dto.StrategyMessageDto;
import com.course.oca.dto.UserLoginReqDto;
import com.course.oca.enums.AuthType;
import com.course.oca.service.strategy.IStrategyDefineService;
import com.course.oca.service.strategy.IStrategyInitService;
import com.course.oca.service.strategy.IStrategyValidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class StrategyValidateServiceImpl implements IStrategyValidateService {

    @Autowired
    private IStrategyInitService strategyInitService;

    @Override
    public List<StrategyMessageDto> beforeAuth(UserLoginReqDto reqDto, UserDetail userDetail) {
        return this.execAuth(reqDto, userDetail, AuthType.BEFORE);
    }

    @Override
    public List<StrategyMessageDto> afterAuth(UserLoginReqDto reqDto, UserDetail userDetail) {
        return this.execAuth(reqDto, userDetail, AuthType.AFTER);
    }

    @Override
    public List<StrategyMessageDto> errorAuth(UserLoginReqDto reqDto, UserDetail userDetail) {
        return this.execAuth(reqDto, userDetail, AuthType.ERROR);
    }

    @Override
    public List<StrategyMessageDto> successAuth(UserLoginReqDto reqDto, UserDetail userDetail) {
        return this.execAuth(reqDto, userDetail, AuthType.SUCCESS);
    }

    @Override
    public List<StrategyMessageDto> passwdAuth(UserLoginReqDto reqDto, UserDetail userDetail) {
        return this.execAuth(reqDto, userDetail, AuthType.PASSWD);
    }

    /**
     * 执行认证策略
     * @param reqDto
     * @param userDetail
     * @param authType
     * @return
     */
    private List<StrategyMessageDto> execAuth(UserLoginReqDto reqDto, UserDetail userDetail, AuthType authType) {
        List<IStrategyDefineService> strategyDefines = this.loadStrategyDefine(reqDto.getSysId(), authType);
        if(strategyDefines != null && strategyDefines.size()>0){
            List<StrategyMessageDto> strategyMessageDtos = new ArrayList<>();
            for(IStrategyDefineService sds : strategyDefines){
                if(sds.isEnable()){
                    StrategyMessageDto strategyMessageDto = sds.validate(reqDto, userDetail);
                    if(strategyMessageDto != null){
                        strategyMessageDtos.add(strategyMessageDto);
                        //非警告类策略，终止策略验证
                        if (!BaseConstants.USER_STATE_WARNING.equals(strategyMessageDto.getAction())) {
                            break;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * 获取认证策略
     * @param sysId
     * @param authType
     * @return
     */
    private List<IStrategyDefineService> loadStrategyDefine(String sysId, AuthType authType) {
        Map<AuthType, List<IStrategyDefineService>> strategyDefines = strategyInitService.initStrategy(sysId);
        if(strategyDefines != null){
            return strategyDefines.get(authType);
        }
        return null;
    }
}
