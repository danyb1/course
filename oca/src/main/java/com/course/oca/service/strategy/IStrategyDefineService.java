package com.course.oca.service.strategy;

import com.course.oca.domain.StrategyDetail;
import com.course.oca.domain.UserDetail;
import com.course.oca.dto.StrategyMessageDto;
import com.course.oca.dto.UserLoginReqDto;
import com.course.oca.enums.AuthType;

/**
 * 认证策略定义
 */
public interface IStrategyDefineService {
    /**
     * 策略验证
     * @param reqDto
     * @param userDetail
     * @return
     */
    public StrategyMessageDto validate(UserLoginReqDto reqDto, UserDetail userDetail);

    /**
     * 认证类型
     * @return
     */
    public AuthType getAuthType();

    /**
     * 认证策略名称
     * @return
     */
    public String getStrategyName();

    /**
     * 策略初始化
     * @param stra
     */
    public void initStrategy(StrategyDetail stra);

    /**
     * 策略是否开启
     * @return
     */
    public boolean isEnable();
}
