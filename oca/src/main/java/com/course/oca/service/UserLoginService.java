package com.course.oca.service;

import com.course.oca.constants.BaseConstants;
import com.course.oca.domain.UserDetail;
import com.course.oca.dto.StrategyMessageDto;
import com.course.oca.dto.UserLoginRepDto;
import com.course.oca.dto.UserLoginReqDto;
import com.course.oca.exception.BusinessException;
import com.course.oca.exception.BusinessExceptionCode;
import com.course.oca.service.auth.IUserAuthService;
import com.course.oca.service.strategy.IStrategyValidateService;
import com.course.oca.util.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserLoginService {

    @Autowired
    private IStrategyValidateService strategyValidateService;

    /**
     * oca执行安全验证（UAA执行安全验证时，把此方法内容分两个接口执行）
     * @param userDto
     * @return
     */
    public UserLoginRepDto login(UserLoginReqDto userDto){

        //根据认证类型，获取授权实现
        IUserAuthService authService = SpringContextUtil.getBean(userDto.getLoginType()+ BaseConstants.USER_AUTH_SERVICE_NAME);
        String loginCode = authService.getLoginCode(userDto);
        if(StringUtils.isEmpty(loginCode)){
            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
        }else{
            userDto.setLoginCode(loginCode);
        }
        //登录用户状态、机构状态判断
        UserDetail userDetail = this.checkUserState(loginCode);
        this.checkUserOrgState(loginCode);

        //前置认证策略验证
        UserLoginRepDto userLoginRepDto = this.beforeAuth(userDto, userDetail);
        if(!userLoginRepDto.getResult()){
            return userLoginRepDto;
        }

        //认证信息验证
        if(!authService.auth(userDto)) {
            strategyValidateService.errorAuth(userDto, userDetail);
            authService.authFailed(userDto);
        }else{
            strategyValidateService.successAuth(userDto, userDetail);
            userLoginRepDto.setResult(true);
        }
        strategyValidateService.afterAuth(userDto, userDetail);
        return userLoginRepDto;
    }

    private UserLoginRepDto beforeAuth(UserLoginReqDto userDto, UserDetail userDetail){
        UserLoginRepDto userLoginRepDto = new UserLoginRepDto();
        List<StrategyMessageDto> authDetails = strategyValidateService.beforeAuth(userDto, userDetail);
        if(authDetails != null){
            List<StrategyMessageDto> errorAuthDetails = authDetails.stream().filter(a->!BaseConstants.USER_STATE_WARNING.equals(a.getAction())).collect(Collectors.toList());
            if(!errorAuthDetails.isEmpty()){//包含非警告类策略时
                userLoginRepDto.setResult(false);
                userLoginRepDto.setStrategyMessage(errorAuthDetails);
            }else{
                userLoginRepDto.setStrategyMessage(authDetails);
            }
        }
        return userLoginRepDto;
    }

    private void checkUserOrgState(String loginCode) {
    }

    private UserDetail checkUserState(String loginCode) {
        return new UserDetail();
    }
}
