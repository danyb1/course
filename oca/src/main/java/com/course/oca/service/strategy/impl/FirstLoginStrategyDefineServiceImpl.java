package com.course.oca.service.strategy.impl;

import com.course.oca.constants.BaseConstants;
import com.course.oca.domain.StrategyDetail;
import com.course.oca.domain.UserDetail;
import com.course.oca.dto.StrategyMessageDto;
import com.course.oca.dto.UserLoginReqDto;
import com.course.oca.enums.AuthType;
import com.course.oca.service.strategy.IStrategyDefineService;
import org.springframework.stereotype.Component;

@Component
public class FirstLoginStrategyDefineServiceImpl extends AbstractDefineServiceImpl {
    @Override
    public StrategyMessageDto validate(UserLoginReqDto reqDto, UserDetail userDetail) {
        return null;
    }

    @Override
    public AuthType getAuthType() {
        return AuthType.BEFORE;
    }

    @Override
    public String getStrategyName() {
        return BaseConstants.LOGIN_FIRST_RULE;
    }

    @Override
    public void initStrategy(StrategyDetail stra) {
        super.initStrategy(stra);
        if(!BaseConstants.ENABLE_FLAG_TRUE.equals(stra.getCrelDetail())){
            this.setEnable(false);
        }
    }
}
