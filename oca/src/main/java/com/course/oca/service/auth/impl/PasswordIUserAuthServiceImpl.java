package com.course.oca.service.auth.impl;

import com.course.oca.dto.UserLoginReqDto;
import com.course.oca.exception.BusinessException;
import com.course.oca.exception.BusinessExceptionCode;
import com.course.oca.service.auth.IUserAuthService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 密码安全验证方式实现
 */
@Component("passwordUserAuthService")
public class PasswordIUserAuthServiceImpl implements IUserAuthService {

    @Override
    public String getLoginCode(UserLoginReqDto userDto) throws BusinessException {
        if(StringUtils.isEmpty(userDto.getLoginCode())){
            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
        }
        return userDto.getLoginCode();
    }

    @Override
    public boolean auth(UserLoginReqDto userDto) {
        return true;
    }

    @Override
    public void authFailed(UserLoginReqDto userDto) throws BusinessException {
        throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
    }
}
