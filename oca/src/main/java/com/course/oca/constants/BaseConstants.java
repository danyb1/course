package com.course.oca.constants;

public class BaseConstants {
    /**
     * 验证服务名称后缀
     */
    public static final String USER_AUTH_SERVICE_NAME = "UserAuthService";

    /**
     * 用户状态冻结
     */
    public static final String USER_STATE_FREEZING = "1";
    /**
     * 用户状态禁止
     */
    public static final String USER_STATE_FORBIDDEN = "2";
    /**
     * 用户状态警告
     */
    public static final String USER_STATE_WARNING = "3";

    /**
     * 策略启用
     */
    public static final String ENABLE_FLAG_TRUE = "1";

    /**
     * 策略未启用
     */
    public static final String ENABLE_FLAG_FALSE = "2";

    /**
     * 首次登陆策略
     */
    public static final String LOGIN_FIRST_RULE = "LOGIN_FIRST_RULE";
    /**
     * 常用IP策略
     */
    public static final String LOGIN_IP_RULE = "LOGIN_IP_RULE";
    /**
     * 在线用户策略
     */
    public static final String LOGIN_USERNUM_RULE = "LOGIN_USERNUM_RULE";
    /**
     * 登陆时间段策略
     */
    public static final String LOGIN_TIME_RULE = "LOGIN_TIME_RULE";
    /**
     * 强制密码修改策略
     */
    public static final String PASSWD_CHANGE_RULE = "PASSWD_CHANGE_RULE";
    /**
     * 密码复杂度策略
     */
    public static final String PASSWD_COMPLEX_RULE = "PASSWD_COMPLEX_RULE";
    /**
     * 密码错误次数策略
     */
    public static final String PASSWD_ERROR_RULE = "PASSWD_ERROR_RULE";
    /**
     * 密码长度策略
     */
    public static final String PASSWD_LENGTH_RULE = "PASSWD_LENGTH_RULE";
    /**
     * 密码重复策略
     */
    public static final String PASSWD_REPETCHG_RULE = "PASSWD_REPETCHG_RULE";
    /**
     * 密码重复字符策略
     */
    public static final String PASSWD_REPETNUMBER_RULE = "PASSWD_REPETNUMBER_RULE";
    /**
     * 密码连续字符策略
     */
    public static final String PASSWD_SEQUNNUMBER_RULE = "PASSWD_SEQUNNUMBER_RULE";
    /**
     * 多设备登录策略
     */
    public static final String LOGIN_TIME_MORE = "LOGIN_TIME_MORE";
    /**
     * 渠道互斥登录策略
     */
    public static final String CHANNEL_EXCLUSIVE_RULE = "CHANNEL_EXCLUSIVE_RULE";
    /**
     * 逻辑系统互斥登录策略
     */
    public static final String LOGIC_SYS_EXCLUSIVE_RULE = "LOGIC_SYS_EXCLUSIVE_RULE";
    /**
     * 冻结策略
     */
    public static final String FROZEN_RULE = "FROZEN_RULE";

}
