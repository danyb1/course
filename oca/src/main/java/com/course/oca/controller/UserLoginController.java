package com.course.oca.controller;

import com.course.oca.dto.ResponseDto;
import com.course.oca.dto.UserLoginRepDto;
import com.course.oca.dto.UserLoginReqDto;
import com.course.oca.exception.BusinessException;
import com.course.oca.exception.BusinessExceptionCode;
import com.course.oca.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserLoginController {

    @Autowired
    private UserLoginService userLoginService;

    @PostMapping("/login")
    public ResponseDto login(@RequestBody UserLoginReqDto userLoginReqDto){
        ResponseDto responseDto = new ResponseDto();
        try {
            UserLoginRepDto userLoginRepDto = userLoginService.login(userLoginReqDto);
            responseDto.setData(userLoginRepDto);
        }catch (BusinessException e){
            UserLoginRepDto userLoginRepDto = new UserLoginRepDto();
            userLoginRepDto.setResult(false);
            userLoginRepDto.setCode(e.getCode());
            userLoginRepDto.setMessage(e.getMessage());

            responseDto.setData(userLoginRepDto);
        }catch (Exception all){
            responseDto.setCode(BusinessExceptionCode.LOGIN_ERROR.getCode());
            responseDto.setMessage(BusinessExceptionCode.LOGIN_ERROR.getDesc());
        }
        return responseDto;
    }

    @PostMapping("/loadUser")
    public ResponseDto loadUser(){
        return null;
    }

    @PostMapping("updateUser")
    public ResponseDto updateUser(){
        return null;
    }
}
