package com.course.oca.domain;

import java.io.Serializable;
import java.util.Objects;

public class StrategyDetail implements Serializable,Cloneable {
    private String crelId;
    private String sysId;
    private String cerlName;
    private String enableFlag;
    private String crelDetail;
    private String actionType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StrategyDetail that = (StrategyDetail) o;
        return Objects.equals(crelId, that.crelId) &&
                Objects.equals(sysId, that.sysId) &&
                Objects.equals(cerlName, that.cerlName) &&
                Objects.equals(enableFlag, that.enableFlag) &&
                Objects.equals(crelDetail, that.crelDetail) &&
                Objects.equals(actionType, that.actionType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(crelId, sysId, cerlName, enableFlag, crelDetail, actionType);
    }

    public String getCrelId() {
        return crelId;
    }

    public void setCrelId(String crelId) {
        this.crelId = crelId;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getCerlName() {
        return cerlName;
    }

    public void setCerlName(String cerlName) {
        this.cerlName = cerlName;
    }

    public String getEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(String enableFlag) {
        this.enableFlag = enableFlag;
    }

    public String getCrelDetail() {
        return crelDetail;
    }

    public void setCrelDetail(String crelDetail) {
        this.crelDetail = crelDetail;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
}
