package com.course.oca.domain;

public class UserDetail {
    private String loginCode;
    private String password;
    private String userId;
    private String offenIP;
    private String lastEditPassTime;
    private String lastLoginTime;
    private String userSts;

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOffenIP() {
        return offenIP;
    }

    public void setOffenIP(String offenIP) {
        this.offenIP = offenIP;
    }

    public String getLastEditPassTime() {
        return lastEditPassTime;
    }

    public void setLastEditPassTime(String lastEditPassTime) {
        this.lastEditPassTime = lastEditPassTime;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getUserSts() {
        return userSts;
    }

    public void setUserSts(String userSts) {
        this.userSts = userSts;
    }
}
