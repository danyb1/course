package com.course.oca.enums;

public enum AuthType {
    BEFORE, AFTER, ERROR, SUCCESS, PASSWD
}
