package com.course.wujp4webserver.dto;

import lombok.Data;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/21/ 14:55
 */

/**
 * 账号信息
 */
@Data
public class Credentials {

    private String username;

    private String password;

}

