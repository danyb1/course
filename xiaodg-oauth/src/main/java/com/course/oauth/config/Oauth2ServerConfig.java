package com.course.oauth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import javax.sql.DataSource;

/**
 * 认证服务器配置
 */
@Configuration
@EnableAuthorizationServer
public class Oauth2ServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * 第1步配置
     * 配置客户端
     * <p>
     * 配置哪些客户端可以访问哪些应用
     *
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        // 可以从数据库中加载
        // clients.jdbc(DataSource);

        // 下面是手动写死的
        clients
                .inMemory()
                .withClient("orderApp") // 客户端
                .secret(passwordEncoder.encode("123456"))
                .scopes("read", "write") // 访问权限
                .accessTokenValiditySeconds(3600)
                .resourceIds("order-server") // 应用（资源服务器），可以填写多个
                .authorizedGrantTypes("password")
                .and()
                .withClient("orderService")
                .secret(passwordEncoder.encode("123456"))
                .scopes("read")
                .accessTokenValiditySeconds(3600)
                .resourceIds("order-server")
                .authorizedGrantTypes("password")
                .and()
                .withClient("gateway") // 配置网关，供网关调用
                .secret(passwordEncoder.encode("123456"))
                .scopes("read", "write")
                .accessTokenValiditySeconds(3600)
                .resourceIds("gateway-server")
                .authorizedGrantTypes("password");
    }

    // 把token存储在Redis中的配置
//    @Bean
//    public TokenStore tokenStore() {
//        RedisTokenStore redisTokenStore = new RedisTokenStore();
//        return redisTokenStore;
//    }

    /**
     * 第2步配置
     * 配置校验用户信息
     *
     * @param endpoints
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints.authenticationManager(authenticationManager);
        // 如果设置了token的存储位置为redis或mysql需要使用下面的代码
        // endpoints.tokenStore(tokenStore())
    }

    /**
     * 第5步配置
     * <p>
     * 在什么情况下进行身份认证
     *
     * @param security
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        // 校验token之前，这个请求一定是经过身份认证了的，即使用了上面的客户端用户和密码的
        security.checkTokenAccess("isAuthenticated()");
    }
}
