module.exports = {
    devServer: {
        disableHostCheck: true,
        proxy: {
            '/wujp4webserver': {
                target: 'http://admin.imooc.com:7004',
                changeOrigin: true,
                // pathRewrite: {
                //     '^/yuspwebserver': ''
                // }
            }
        }
    }
}