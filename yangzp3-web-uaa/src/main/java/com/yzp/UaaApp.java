package com.yzp;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/18 17:26
 */
@Slf4j
@SpringBootApplication
public class UaaApp {

    public static void main(String[] args) {

        Environment environment = SpringApplication.run(UaaApp.class, args).getEnvironment();
        log.info("Uaa已启动， port="+ environment.getProperty("server.port"));
    }
}
