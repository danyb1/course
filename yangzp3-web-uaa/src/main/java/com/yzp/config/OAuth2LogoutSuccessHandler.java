package com.yzp.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/18 18:32
 */
@Component
public class OAuth2LogoutSuccessHandler implements LogoutSuccessHandler {
    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        //String redirectUrl = httpServletRequest.getParameter("redirect_url");
        final String redirectUrl = "http://admin.imooc.com:8080/login";
        if(!StringUtils.isEmpty(redirectUrl)){
            httpServletResponse.sendRedirect(redirectUrl);
        }
    }
}


























