package com.yzp.config;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * 系统用户表(AdminSmUser)实体类
 *
 * @author makejava
 * @since 2020-09-22 18:25:52
 */
@Data
class User  implements UserDetails {
    private static final long serialVersionUID = -68224585251723869L;

    @NotBlank
    private long id;
    @NotBlank
    @Pattern(regexp = "^\\d{1,255}\\w{1,255}[a-z]*[A-Z]*[0-9]+$")
    private String name;
    @NotEmpty
    @NotBlank
    private String password;
    @NotEmpty
    private String permissions;
    @NotNull
    @NotBlank
    private String username;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<SimpleGrantedAuthority> list = new ArrayList<>();
        list.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        return list;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}