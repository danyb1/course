package entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 系统用户表(AdminSmUser)实体类
 *
 * @author makejava
 * @since 2020-09-22 18:25:52
 */
@Data
public class MyUser implements Serializable {
    private static final long serialVersionUID = -68224585251723869L;

    private long id;
    private String name;
    private String password;
    private String permissions;
    private String username;


}