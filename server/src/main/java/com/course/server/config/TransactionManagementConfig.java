package com.course.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author danyu
 */
@EnableTransactionManagement
@Configuration
public class TransactionManagementConfig {
	
}