package com.course.server.service;

import com.course.server.domain.Oauthclientdetails;
import com.course.server.domain.OauthclientdetailsExample;
import com.course.server.dto.OauthclientdetailsDto;
import com.course.server.dto.PageDto;
import com.course.server.mapper.OauthclientdetailsMapper;
import com.course.server.util.CopyUtil;
import com.course.server.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OauthclientdetailsService {

    @Resource
    private OauthclientdetailsMapper oauthclientdetailsMapper;

    /**
     * 列表查询
     */
    public void list(PageDto pageDto) {
        PageHelper.startPage(pageDto.getPage(), pageDto.getSize());
        OauthclientdetailsExample oauthclientdetailsExample = new OauthclientdetailsExample();
        List<Oauthclientdetails> oauthclientdetailsList = oauthclientdetailsMapper.selectByExample(oauthclientdetailsExample);
        PageInfo<Oauthclientdetails> pageInfo = new PageInfo<>(oauthclientdetailsList);
        pageDto.setTotal(pageInfo.getTotal());
        List<OauthclientdetailsDto> oauthclientdetailsDtoList = CopyUtil.copyList(oauthclientdetailsList, OauthclientdetailsDto.class);
        pageDto.setList(oauthclientdetailsDtoList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     */
    public void save(OauthclientdetailsDto oauthclientdetailsDto) {
        Oauthclientdetails oauthclientdetails = CopyUtil.copy(oauthclientdetailsDto, Oauthclientdetails.class);
        if (StringUtils.isEmpty(oauthclientdetailsDto.getId())) {
            this.insert(oauthclientdetails);
        } else {
            this.update(oauthclientdetails);
        }
    }

    /**
     * 新增
     */
    private void insert(Oauthclientdetails oauthclientdetails) {
        oauthclientdetails.setId(UuidUtil.getShortUuid());
        oauthclientdetailsMapper.insert(oauthclientdetails);
    }

    /**
     * 更新
     */
    private void update(Oauthclientdetails oauthclientdetails) {
        oauthclientdetailsMapper.updateByPrimaryKey(oauthclientdetails);
    }

    /**
     * 删除
     */
    public void delete(String id) {
        oauthclientdetailsMapper.deleteByPrimaryKey(id);
    }

}
