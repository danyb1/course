package com.course.server.mapper;

import com.course.server.domain.Oauthclientdetails;
import com.course.server.domain.OauthclientdetailsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OauthclientdetailsMapper {
    long countByExample(OauthclientdetailsExample example);

    int deleteByExample(OauthclientdetailsExample example);

    int deleteByPrimaryKey(String clientId);

    int insert(Oauthclientdetails record);

    int insertSelective(Oauthclientdetails record);

    List<Oauthclientdetails> selectByExample(OauthclientdetailsExample example);

    Oauthclientdetails selectByPrimaryKey(String clientId);

    int updateByExampleSelective(@Param("record") Oauthclientdetails record, @Param("example") OauthclientdetailsExample example);

    int updateByExample(@Param("record") Oauthclientdetails record, @Param("example") OauthclientdetailsExample example);

    int updateByPrimaryKeySelective(Oauthclientdetails record);

    int updateByPrimaryKey(Oauthclientdetails record);
}