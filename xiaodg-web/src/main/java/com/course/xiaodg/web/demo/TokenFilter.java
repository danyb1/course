package com.course.xiaodg.web.demo;

import com.course.xiaodg.web.demo.dto.Token;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 适用ZULL代理道网关的请求头中加入token
 */
@Component
public class TokenFilter extends ZuulFilter {
    private static final Logger logger = LoggerFactory.getLogger(TokenFilter.class);

    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        if (!request.getRequestURI().startsWith("/token")) {
            // 登录的放过
            Token token = (Token) request.getSession().getAttribute("token");
            if (token != null) {
                String tokenVal = token.getAccess_token();
                // 判断Token是否过期，如果已过期，进行刷新Token操作
                if (token.isExpired()) {
                    String oauthServiceUrl = "http://localhost:9070/token/oauth/token";
                    HttpHeaders httpHeaders = new HttpHeaders();
                    httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

                    // Admin应用的clientId和clientSecret
                    httpHeaders.setBasicAuth("admin", "123456");
                    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
                    params.add("grant_type", "refresh_token");
                    // 这个redirect_url参数要与index.html中传递的一致，不一致会报错
                    params.add("refresh_token", token.getRefresh_token());
                    HttpEntity<MultiValueMap<String, String>> multiValueMapHttpEntity = new HttpEntity<>(params, httpHeaders);
                    try {
                        ResponseEntity<Token> tokenResponse = restTemplate.exchange(oauthServiceUrl, HttpMethod.POST, multiValueMapHttpEntity, Token.class);
                        Token newToken = tokenResponse.getBody().init();
                        logger.info("newToken: " + newToken);
                        request.getSession().setAttribute("token", newToken);
                        tokenVal = newToken.getAccess_token();
                    } catch (Exception e) {
                        // token已过期，使用刷新token获取token失败，在网关正常的情况下说明是刷新token也过期了，用户需要重新登录了
                        requestContext.setSendZuulResponse(false);
                        requestContext.setResponseStatusCode(401);
                        requestContext.setResponseBody("{\"message\": \"logedout\"}");
                        requestContext.getResponse().setContentType(MediaType.APPLICATION_JSON_VALUE);
                    }
                }
                requestContext.addZuulRequestHeader("Authorization", "Bearer " + tokenVal);
            } else {
                handleError(401, requestContext);
            }
        }
        return null;
    }

    private void handleError(int responseCode, RequestContext requestContext) {
        HttpServletResponse response = requestContext.getResponse();
        response.setStatus(responseCode);
        response.setContentType("application/json");
        requestContext.setResponseBody("{\"message\": \"auth failed\"}");
        // 不向业务服务继续调用，直接返回给客户端
        requestContext.setSendZuulResponse(false);
    }
}
