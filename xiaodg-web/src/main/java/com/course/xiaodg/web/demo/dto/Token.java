package com.course.xiaodg.web.demo.dto;

import java.time.LocalDateTime;

public class Token {
    private String access_token;
    private String refresh_token;
    private String token_type;
    private Long expires_in;
    private String scope;
    LocalDateTime expiredTime;


    public Token init() {
        // -3的意思是出去token生成所花费的时间，这是个预估值
        this.expiredTime = LocalDateTime.now().plusSeconds(this.expires_in - 3);
        return this;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public Long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Long expires_in) {
        this.expires_in = expires_in;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @Override
    public String toString() {
        return "Token{" +
                "access_token='" + access_token + '\'' +
                ", refresh_token='" + refresh_token + '\'' +
                ", token_type='" + token_type + '\'' +
                ", expires_in=" + expires_in +
                ", scope='" + scope + '\'' +
                '}';
    }


    public boolean isExpired() {
        return expiredTime.isBefore(LocalDateTime.now());
    }
}
