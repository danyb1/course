package com.course.xiaodg.web.demo.rest;

import com.course.xiaodg.web.demo.dto.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping
public class AdminController {

    private RestTemplate restTemplate = new RestTemplate();

    private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @PostMapping("/logout")
    public void logout(HttpServletRequest request) {
        request.getSession().invalidate();
    }

    @GetMapping("/oauth/callback")
    public void oauthCallback(@RequestParam String code, @RequestParam String state, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("code=" + code + ", state=" + state);

        String oauthServiceUrl = "http://localhost:9070/token/oauth/token";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        // Admin应用的clientId和clientSecret
        httpHeaders.setBasicAuth("admin", "123456");
        // 回传参数
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("code", code);
        params.add("grant_type", "authorization_code");
        // 这个redirect_url参数要与index.html中传递的一致，不一致会报错
        params.add("redirect_uri", "http://localhost:9999/oauth/callback");

        HttpEntity<MultiValueMap<String, String>> multiValueMapHttpEntity = new HttpEntity<>(params, httpHeaders);

        ResponseEntity<Token> tokenResponse = restTemplate.exchange(oauthServiceUrl, HttpMethod.POST, multiValueMapHttpEntity, Token.class);

        Token token = tokenResponse.getBody().init();
        logger.info("token: " + token);
        request.getSession().setAttribute("token", token);

        response.sendRedirect("/index.html");
    }

    @GetMapping("/me")
    public Token me(HttpServletRequest request) {
        return (Token) request.getSession().getAttribute("token");
    }
}
