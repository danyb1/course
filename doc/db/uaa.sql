-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details`
(
    `id`                      char(8)       not null comment 'id',
    `client_id`               varchar(256)  NOT NULL comment '客户端id',
    `resource_ids`            varchar(256)  NULL DEFAULT NULL comment '资源ids|留空表示不受限',
    `client_secret`           varchar(256)  NULL DEFAULT NULL comment '客户端密码',
    `scope`                   varchar(256)  NULL DEFAULT NULL comment '客户端权限|read write',
    `authorized_grant_types`  varchar(256)  NULL DEFAULT NULL comment '授权类型|password,authorization_code,refresh_token,客户端模式，隐式模式',
    `web_server_redirect_uri` varchar(256)  NULL DEFAULT NULL comment '重定向url',
    `authorities`             varchar(256)  NULL DEFAULT NULL comment '用户权限',
    `access_token_validity`   int(0)        NULL DEFAULT NULL comment '访问令牌有效期|单位秒',
    `refresh_token_validity`  int(0)        NULL DEFAULT NULL comment '刷新令牌有效期|单位秒',
    `additional_information`  varchar(4096) NULL DEFAULT NULL comment '附加信息',
    `autoapprove`             varchar(256)  NULL DEFAULT NULL comment 'autoapprove|true or false',
    PRIMARY KEY (`client_id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 comment '客户端oauth明细表';
