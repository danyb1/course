package com.course.wujp4webuaa;

import com.course.wujp4webuaa.service.UserFeignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.core.env.Environment;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/21/ 15:12
 */
@SpringBootApplication(scanBasePackages = "com.course")
@EnableEurekaClient
@EnableDiscoveryClient
@EnableFeignClients
public class Wujp4WebUaaApplication {


    private static final Logger LOG = LoggerFactory.getLogger(Wujp4WebUaaApplication.class);

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Wujp4WebUaaApplication.class);
        Environment env = app.run(args).getEnvironment();
        LOG.info("启动成功！！");
        LOG.info("Wujp4WebUaa地址: \thttp://127.0.0.1:{}", env.getProperty("server.port"));
    }
}