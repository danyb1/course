package com.course.wujp4webuaa.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/25/ 14:15
 */
@Configuration
public class FeignConfig {
    @Bean
    Logger.Level feignLoggerLever(){
        return Logger.Level.FULL;
    }
}
