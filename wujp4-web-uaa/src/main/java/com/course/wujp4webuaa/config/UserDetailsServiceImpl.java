/**
 * 
 */
package com.course.wujp4webuaa.config;

import com.course.wujp4webuaa.dto.ResponseDto;
import com.course.wujp4webuaa.dto.UserLoginDto;
import com.course.wujp4webuaa.service.UserFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.sql.SQLOutput;

/**
 * @author jojo
 *
 */
@Component
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserFeignService userFeignService;

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("输入的用户名："+username);
		ResponseDto<UserLoginDto> responseDto=userFeignService.getUserLoginByUsercode(username);
		if(!responseDto.getSuccess()){
			throw new UsernameNotFoundException(responseDto.getMessage());
		}else{
			UserLoginDto userLoginDto=responseDto.getContent();
			System.out.println(userLoginDto);
			System.out.println(userLoginDto.getUserPassword());
			return User.withUsername(userLoginDto.getUserCode())
					.password(userLoginDto.getUserPassword())
					.authorities("ROLE_ADMIN")
					.build();
		}
	}

}
