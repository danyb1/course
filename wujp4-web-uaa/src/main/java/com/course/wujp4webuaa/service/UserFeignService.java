package com.course.wujp4webuaa.service;

import com.course.wujp4webuaa.dto.ResponseDto;
import com.course.wujp4webuaa.dto.UserLoginDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/24/ 19:38
 */
@Component
@FeignClient(value="WUJP4WEBOCA")
public interface UserFeignService {
//    @GetMapping(value="/user/getUser")
//    public UserLoginDto getUserLoginByUsercode(@RequestParam(name ="usercode") String usercode);

    @GetMapping(value="/user/getUser")
    public ResponseDto<UserLoginDto> getUserLoginByUsercode(@RequestParam(name ="usercode") String usercode);

}

