package com.course.yuspwebgateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.core.env.Environment;

@SpringBootApplication(scanBasePackages = "com.course")
@EnableZuulProxy
public class YuspWebGatewayApplication {

    private static final Logger LOG = LoggerFactory.getLogger(YuspWebGatewayApplication.class);

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(YuspWebGatewayApplication.class);
        Environment env = app.run(args).getEnvironment();
        LOG.info("启动成功！！");
        LOG.info("YuspWebGateway地址: \thttp://127.0.0.1:{}", env.getProperty("server.port"));
    }

}
