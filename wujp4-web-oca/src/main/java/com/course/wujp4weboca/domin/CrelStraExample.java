package com.course.wujp4weboca.domin;

import java.util.ArrayList;
import java.util.List;

public class CrelStraExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CrelStraExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSysIdIsNull() {
            addCriterion("sys_id is null");
            return (Criteria) this;
        }

        public Criteria andSysIdIsNotNull() {
            addCriterion("sys_id is not null");
            return (Criteria) this;
        }

        public Criteria andSysIdEqualTo(String value) {
            addCriterion("sys_id =", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdNotEqualTo(String value) {
            addCriterion("sys_id <>", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdGreaterThan(String value) {
            addCriterion("sys_id >", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdGreaterThanOrEqualTo(String value) {
            addCriterion("sys_id >=", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdLessThan(String value) {
            addCriterion("sys_id <", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdLessThanOrEqualTo(String value) {
            addCriterion("sys_id <=", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdLike(String value) {
            addCriterion("sys_id like", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdNotLike(String value) {
            addCriterion("sys_id not like", value, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdIn(List<String> values) {
            addCriterion("sys_id in", values, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdNotIn(List<String> values) {
            addCriterion("sys_id not in", values, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdBetween(String value1, String value2) {
            addCriterion("sys_id between", value1, value2, "sysId");
            return (Criteria) this;
        }

        public Criteria andSysIdNotBetween(String value1, String value2) {
            addCriterion("sys_id not between", value1, value2, "sysId");
            return (Criteria) this;
        }

        public Criteria andCrelNameIsNull() {
            addCriterion("crel_name is null");
            return (Criteria) this;
        }

        public Criteria andCrelNameIsNotNull() {
            addCriterion("crel_name is not null");
            return (Criteria) this;
        }

        public Criteria andCrelNameEqualTo(String value) {
            addCriterion("crel_name =", value, "crelName");
            return (Criteria) this;
        }

        public Criteria andCrelNameNotEqualTo(String value) {
            addCriterion("crel_name <>", value, "crelName");
            return (Criteria) this;
        }

        public Criteria andCrelNameGreaterThan(String value) {
            addCriterion("crel_name >", value, "crelName");
            return (Criteria) this;
        }

        public Criteria andCrelNameGreaterThanOrEqualTo(String value) {
            addCriterion("crel_name >=", value, "crelName");
            return (Criteria) this;
        }

        public Criteria andCrelNameLessThan(String value) {
            addCriterion("crel_name <", value, "crelName");
            return (Criteria) this;
        }

        public Criteria andCrelNameLessThanOrEqualTo(String value) {
            addCriterion("crel_name <=", value, "crelName");
            return (Criteria) this;
        }

        public Criteria andCrelNameLike(String value) {
            addCriterion("crel_name like", value, "crelName");
            return (Criteria) this;
        }

        public Criteria andCrelNameNotLike(String value) {
            addCriterion("crel_name not like", value, "crelName");
            return (Criteria) this;
        }

        public Criteria andCrelNameIn(List<String> values) {
            addCriterion("crel_name in", values, "crelName");
            return (Criteria) this;
        }

        public Criteria andCrelNameNotIn(List<String> values) {
            addCriterion("crel_name not in", values, "crelName");
            return (Criteria) this;
        }

        public Criteria andCrelNameBetween(String value1, String value2) {
            addCriterion("crel_name between", value1, value2, "crelName");
            return (Criteria) this;
        }

        public Criteria andCrelNameNotBetween(String value1, String value2) {
            addCriterion("crel_name not between", value1, value2, "crelName");
            return (Criteria) this;
        }

        public Criteria andEnableFlagIsNull() {
            addCriterion("enable_flag is null");
            return (Criteria) this;
        }

        public Criteria andEnableFlagIsNotNull() {
            addCriterion("enable_flag is not null");
            return (Criteria) this;
        }

        public Criteria andEnableFlagEqualTo(String value) {
            addCriterion("enable_flag =", value, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andEnableFlagNotEqualTo(String value) {
            addCriterion("enable_flag <>", value, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andEnableFlagGreaterThan(String value) {
            addCriterion("enable_flag >", value, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andEnableFlagGreaterThanOrEqualTo(String value) {
            addCriterion("enable_flag >=", value, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andEnableFlagLessThan(String value) {
            addCriterion("enable_flag <", value, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andEnableFlagLessThanOrEqualTo(String value) {
            addCriterion("enable_flag <=", value, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andEnableFlagLike(String value) {
            addCriterion("enable_flag like", value, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andEnableFlagNotLike(String value) {
            addCriterion("enable_flag not like", value, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andEnableFlagIn(List<String> values) {
            addCriterion("enable_flag in", values, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andEnableFlagNotIn(List<String> values) {
            addCriterion("enable_flag not in", values, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andEnableFlagBetween(String value1, String value2) {
            addCriterion("enable_flag between", value1, value2, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andEnableFlagNotBetween(String value1, String value2) {
            addCriterion("enable_flag not between", value1, value2, "enableFlag");
            return (Criteria) this;
        }

        public Criteria andActionTypeIsNull() {
            addCriterion("action_type is null");
            return (Criteria) this;
        }

        public Criteria andActionTypeIsNotNull() {
            addCriterion("action_type is not null");
            return (Criteria) this;
        }

        public Criteria andActionTypeEqualTo(String value) {
            addCriterion("action_type =", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeNotEqualTo(String value) {
            addCriterion("action_type <>", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeGreaterThan(String value) {
            addCriterion("action_type >", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeGreaterThanOrEqualTo(String value) {
            addCriterion("action_type >=", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeLessThan(String value) {
            addCriterion("action_type <", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeLessThanOrEqualTo(String value) {
            addCriterion("action_type <=", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeLike(String value) {
            addCriterion("action_type like", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeNotLike(String value) {
            addCriterion("action_type not like", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeIn(List<String> values) {
            addCriterion("action_type in", values, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeNotIn(List<String> values) {
            addCriterion("action_type not in", values, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeBetween(String value1, String value2) {
            addCriterion("action_type between", value1, value2, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeNotBetween(String value1, String value2) {
            addCriterion("action_type not between", value1, value2, "actionType");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrIsNull() {
            addCriterion("last_chg_usr is null");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrIsNotNull() {
            addCriterion("last_chg_usr is not null");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrEqualTo(String value) {
            addCriterion("last_chg_usr =", value, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrNotEqualTo(String value) {
            addCriterion("last_chg_usr <>", value, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrGreaterThan(String value) {
            addCriterion("last_chg_usr >", value, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrGreaterThanOrEqualTo(String value) {
            addCriterion("last_chg_usr >=", value, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrLessThan(String value) {
            addCriterion("last_chg_usr <", value, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrLessThanOrEqualTo(String value) {
            addCriterion("last_chg_usr <=", value, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrLike(String value) {
            addCriterion("last_chg_usr like", value, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrNotLike(String value) {
            addCriterion("last_chg_usr not like", value, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrIn(List<String> values) {
            addCriterion("last_chg_usr in", values, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrNotIn(List<String> values) {
            addCriterion("last_chg_usr not in", values, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrBetween(String value1, String value2) {
            addCriterion("last_chg_usr between", value1, value2, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgUsrNotBetween(String value1, String value2) {
            addCriterion("last_chg_usr not between", value1, value2, "lastChgUsr");
            return (Criteria) this;
        }

        public Criteria andLastChgDtIsNull() {
            addCriterion("last_chg_dt is null");
            return (Criteria) this;
        }

        public Criteria andLastChgDtIsNotNull() {
            addCriterion("last_chg_dt is not null");
            return (Criteria) this;
        }

        public Criteria andLastChgDtEqualTo(String value) {
            addCriterion("last_chg_dt =", value, "lastChgDt");
            return (Criteria) this;
        }

        public Criteria andLastChgDtNotEqualTo(String value) {
            addCriterion("last_chg_dt <>", value, "lastChgDt");
            return (Criteria) this;
        }

        public Criteria andLastChgDtGreaterThan(String value) {
            addCriterion("last_chg_dt >", value, "lastChgDt");
            return (Criteria) this;
        }

        public Criteria andLastChgDtGreaterThanOrEqualTo(String value) {
            addCriterion("last_chg_dt >=", value, "lastChgDt");
            return (Criteria) this;
        }

        public Criteria andLastChgDtLessThan(String value) {
            addCriterion("last_chg_dt <", value, "lastChgDt");
            return (Criteria) this;
        }

        public Criteria andLastChgDtLessThanOrEqualTo(String value) {
            addCriterion("last_chg_dt <=", value, "lastChgDt");
            return (Criteria) this;
        }

        public Criteria andLastChgDtLike(String value) {
            addCriterion("last_chg_dt like", value, "lastChgDt");
            return (Criteria) this;
        }

        public Criteria andLastChgDtNotLike(String value) {
            addCriterion("last_chg_dt not like", value, "lastChgDt");
            return (Criteria) this;
        }

        public Criteria andLastChgDtIn(List<String> values) {
            addCriterion("last_chg_dt in", values, "lastChgDt");
            return (Criteria) this;
        }

        public Criteria andLastChgDtNotIn(List<String> values) {
            addCriterion("last_chg_dt not in", values, "lastChgDt");
            return (Criteria) this;
        }

        public Criteria andLastChgDtBetween(String value1, String value2) {
            addCriterion("last_chg_dt between", value1, value2, "lastChgDt");
            return (Criteria) this;
        }

        public Criteria andLastChgDtNotBetween(String value1, String value2) {
            addCriterion("last_chg_dt not between", value1, value2, "lastChgDt");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}