package com.course.wujp4weboca.domin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLogin {

    private String id;

    private String userCode;

    private String userName;

    private String userPassword;

    private String orgId;

    private String userCreator;

    private Date lastLoginTime;

    private Date lastEditPasswordTime;

    private String offenIP;

    private String userStatus;
}