package com.course.wujp4weboca.domin;

public class CrelStra {
    private String id;

    private String sysId;

    private String crelName;

    private String enableFlag;

    private String actionType;

    private String lastChgUsr;

    private String lastChgDt;

    private String crelDetail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getCrelName() {
        return crelName;
    }

    public void setCrelName(String crelName) {
        this.crelName = crelName;
    }

    public String getEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(String enableFlag) {
        this.enableFlag = enableFlag;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getLastChgUsr() {
        return lastChgUsr;
    }

    public void setLastChgUsr(String lastChgUsr) {
        this.lastChgUsr = lastChgUsr;
    }

    public String getLastChgDt() {
        return lastChgDt;
    }

    public void setLastChgDt(String lastChgDt) {
        this.lastChgDt = lastChgDt;
    }

    public String getCrelDetail() {
        return crelDetail;
    }

    public void setCrelDetail(String crelDetail) {
        this.crelDetail = crelDetail;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", sysId=").append(sysId);
        sb.append(", crelName=").append(crelName);
        sb.append(", enableFlag=").append(enableFlag);
        sb.append(", actionType=").append(actionType);
        sb.append(", lastChgUsr=").append(lastChgUsr);
        sb.append(", lastChgDt=").append(lastChgDt);
        sb.append(", crelDetail=").append(crelDetail);
        sb.append("]");
        return sb.toString();
    }
}