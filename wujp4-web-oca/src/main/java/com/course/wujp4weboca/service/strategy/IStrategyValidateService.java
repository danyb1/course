package com.course.wujp4weboca.service.strategy;

import com.course.wujp4weboca.domin.UserLogin;
import com.course.wujp4weboca.dto.StrategyMessageDto;
import com.course.wujp4weboca.dto.UserLoginReqDto;

import java.util.List;

public interface IStrategyValidateService {

    /**
     * 交易前策略执行
     * @param reqDto
     * @param userLogin
     * @return
     */
    List<StrategyMessageDto> beforeAuth(UserLoginReqDto reqDto, UserLogin userLogin);

    /**
     * 交易后策略执行
     * @param reqDto
     * @param userLogin
     * @return
     */
    List<StrategyMessageDto> afterAuth(UserLoginReqDto reqDto, UserLogin userLogin);

    /**
     * 安全认证失败执行策略
     * @param reqDto
     * @param userLogin
     * @return
     */
    List<StrategyMessageDto> errorAuth(UserLoginReqDto reqDto, UserLogin userLogin);

    /**
     * 安全认证成功执行策略
     * @param reqDto
     * @param userLogin
     * @return
     */
    List<StrategyMessageDto> successAuth(UserLoginReqDto reqDto, UserLogin userLogin);

    /**
     * 密码强度验证策略
     * @param reqDto
     * @param userLogin
     * @return
     */
    List<StrategyMessageDto> passwdAuth(UserLoginReqDto reqDto, UserLogin userLogin);

}
