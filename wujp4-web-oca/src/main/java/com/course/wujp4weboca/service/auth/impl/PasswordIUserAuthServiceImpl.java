package com.course.wujp4weboca.service.auth.impl;

import com.course.wujp4weboca.dto.UserLoginReqDto;
import com.course.wujp4weboca.exception.BusinessException;
import com.course.wujp4weboca.exception.BusinessExceptionCode;
import com.course.wujp4weboca.service.auth.IUserAuthService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 密码安全验证方式实现
 */
@Component("passwordUserAuthService")
public class PasswordIUserAuthServiceImpl implements IUserAuthService {

    @Override
    public String getUserCode(UserLoginReqDto userDto) throws BusinessException {
        if(StringUtils.isEmpty(userDto.getUserCode())){
            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
        }
        return userDto.getUserCode();
    }

    @Override
    public boolean auth(UserLoginReqDto userDto) {
        return true;
    }

    @Override
    public void authFailed(UserLoginReqDto userDto) throws BusinessException {
        throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
    }
}
