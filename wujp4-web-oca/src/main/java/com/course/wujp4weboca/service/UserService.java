package com.course.wujp4weboca.service;

import com.course.wujp4weboca.dto.UserDto;
import org.apache.ibatis.annotations.Param;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/24/ 17:03
 */
public interface UserService {
    UserDto getUserDtoByUsername(@Param("username") String username);
}
