package com.course.wujp4weboca.service.strategy.impl;

import com.course.wujp4weboca.constants.BaseConstants;
import com.course.wujp4weboca.domin.StrategyDetail;
import com.course.wujp4weboca.domin.UserLogin;
import com.course.wujp4weboca.dto.StrategyMessageDto;
import com.course.wujp4weboca.dto.UserLoginReqDto;
import com.course.wujp4weboca.enums.AuthType;
import org.springframework.stereotype.Component;

@Component
public class FirstLoginStrategyDefineServiceImpl extends AbstractDefineServiceImpl {
    @Override
    public StrategyMessageDto validate(UserLoginReqDto reqDto, UserLogin userDetail) {
        return null;
    }

    @Override
    public AuthType getAuthType() {
        return AuthType.BEFORE;
    }

    @Override
    public String getStrategyName() {
        return BaseConstants.LOGIN_FIRST_RULE;
    }

    @Override
    public void initStrategy(StrategyDetail stra) {
        super.initStrategy(stra);
        if(!BaseConstants.ENABLE_FLAG_TRUE.equals(stra.getCrelDetail())){
            this.setEnable(false);
        }
    }
}
