package com.course.wujp4weboca.service.strategy.impl;

import com.course.wujp4weboca.constants.BaseConstants;
import com.course.wujp4weboca.domin.StrategyDetail;
import com.course.wujp4weboca.enums.AuthType;
import com.course.wujp4weboca.service.strategy.IStrategyDefineService;
import com.course.wujp4weboca.service.strategy.IStrategyInitService;
import com.course.wujp4weboca.util.SpringContextUtil;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class StrategyInitServiceImpl implements IStrategyInitService {
    private ConcurrentHashMap<String, Map<AuthType, List<IStrategyDefineService>>> strategyDefineServiceMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, Long> creStrategyInitTimeMap = new ConcurrentHashMap<>();// 存储逻辑系统应用的策略的初始化
    private final static Long CHANGE_TIME = 60000L;// 刷新逻辑系统最小间隔时间(毫秒)
    private Map<String, IStrategyDefineService> implStrategyDefine;
    /**
     * 初始化认证策略
     * @param sysId
     * @return
     */
    @Override
    public Map<AuthType, List<IStrategyDefineService>> initStrategy(String sysId) {
        //根据系统ID初始化认证策略
        //认证策略设置一定缓存时间
//        String creStrategyKey=sysId;
//        if(sysId==null){
//            creStrategyKey= BaseConstants.STRATEGY_KEY_ALL;
//        }
//        Long initTime=this.creStrategyInitTimeMap.get(creStrategyKey);
//        if(initTime==null){
//            initTime=BaseConstants.STRATEGY_INIT_TIME;
//        }
//        Long creStrategyInitTime=System.currentTimeMillis()-initTime;
//        Map<AuthType, List<IStrategyDefineService>> creStrategyMap=this.strategyDefineServiceMap.get(creStrategyKey);
//        if(creStrategyInitTime>CHANGE_TIME || creStrategyMap==null){
//            synchronized (sysId.intern()){
//                if(this.implStrategyDefine == null) {
//                    this.loadAll();
//                }
//                initTime=this.creStrategyInitTimeMap.get(creStrategyKey);
//                if(initTime==null){
//                    initTime=BaseConstants.STRATEGY_INIT_TIME;
//                }
//                creStrategyInitTime=System.currentTimeMillis()-initTime;
//                creStrategyMap=this.strategyDefineServiceMap.get(creStrategyKey);
//                if(creStrategyInitTime>CHANGE_TIME || creStrategyMap==null){
//                    Map<AuthType, List<IStrategyDefineService>> credentialStrategyMap= new HashMap<>();
//
//                    for() {
//                        IStrategyDefineService creStrategy = this.implStrategyDefine.get("First_log");
//                        credentialStrategyMap.put(creStrategy.getAuthType(), creStrategy.clone());
//                    }
//
//                    this.strategyDefineServiceMap.put(creStrategyKey, credentialStrategyMap);
//                }
//
//            }
//        }
        //认证策略加载时，根据认证策略名称与实现类getStrategyName返回值匹配并初始化
        //认证策略动态加载，通过spring容器获取所有实现（在加载时，每个对像需要克隆一份）或通过反射方式获取所有实现并初始化
        //添加新认证策略时，只需要实现对应接口或类，不需要修改公共代码

        return null;
    }

    private void loadAll(){
            String[] icsNames = SpringContextUtil.getBeanNamesForType(IStrategyDefineService.class);
            this.implStrategyDefine = new HashMap<>();
            for (String name : icsNames) {
                IStrategyDefineService creStrategy = SpringContextUtil.getBean(name);
                implStrategyDefine.put(name, creStrategy);
            }
    }
}
