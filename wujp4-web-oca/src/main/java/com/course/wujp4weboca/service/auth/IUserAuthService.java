package com.course.wujp4weboca.service.auth;


import com.course.wujp4weboca.dto.UserLoginReqDto;
import com.course.wujp4weboca.exception.BusinessException;

public interface IUserAuthService {
    /**
     * 根据登录信息获取登录号，登录号不存在时可抛出自定义异常
     * @param userDto
     * @throws BusinessException
     * @return
     */
    public String getUserCode(UserLoginReqDto userDto) throws BusinessException;
    /**
     * 登录授权验证
     * @param userDto
     * @return
     */
    public boolean auth(UserLoginReqDto userDto);

    /**
     * 认证失败抛出异常
     * @param userDto
     * @throws BusinessException
     */
    public void authFailed(UserLoginReqDto userDto) throws BusinessException;
}
