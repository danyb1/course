package com.course.wujp4weboca.service.impl;

import com.course.wujp4weboca.dto.UserDto;
import com.course.wujp4weboca.mapper.UserMapper;
import com.course.wujp4weboca.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/24/ 17:04
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    /**
     * 根据用户名查询用户信息
     * @param username
     * @return
     */
    @Override
    public UserDto getUserDtoByUsername(String username) {
        UserDto userDto=new UserDto();
        BeanUtils.copyProperties(userMapper.getUserByUsername(username),userDto);
        return userDto;
    }
}
