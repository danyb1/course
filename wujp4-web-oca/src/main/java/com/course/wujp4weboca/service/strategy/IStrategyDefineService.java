package com.course.wujp4weboca.service.strategy;

import com.course.wujp4weboca.domin.StrategyDetail;
import com.course.wujp4weboca.domin.UserLogin;
import com.course.wujp4weboca.dto.StrategyMessageDto;
import com.course.wujp4weboca.dto.UserLoginReqDto;
import com.course.wujp4weboca.enums.AuthType;

/**
 * 认证策略定义
 */
public interface IStrategyDefineService {
    /**
     * 策略验证
     * @param reqDto
     * @param userDetail
     * @return
     */
    public StrategyMessageDto validate(UserLoginReqDto reqDto, UserLogin userDetail);

    /**
     * 认证类型
     * @return
     */
    public AuthType getAuthType();

    /**
     * 认证策略名称
     * @return
     */
    public String getStrategyName();

    /**
     * 策略初始化
     * @param stra
     */
    public void initStrategy(StrategyDetail stra);

    /**
     * 策略是否开启
     * @return
     */
    public boolean isEnable();
}
