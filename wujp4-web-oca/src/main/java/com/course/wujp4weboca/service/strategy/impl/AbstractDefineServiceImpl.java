package com.course.wujp4weboca.service.strategy.impl;


import com.course.wujp4weboca.constants.BaseConstants;
import com.course.wujp4weboca.domin.StrategyDetail;
import com.course.wujp4weboca.service.strategy.IStrategyDefineService;

public abstract class AbstractDefineServiceImpl implements IStrategyDefineService {

    /**
     * 开关
     */
    private boolean enable = false;
    /**
     * 执行动作
     */
    protected String actionType;
    /**
     * 策略标识
     */
    public String creId;
    /**
     * 策略明细
     */
    protected String creDetail;

    @Override
    public void initStrategy(StrategyDetail stra) {
        this.setEnable(BaseConstants.ENABLE_FLAG_TRUE.equals(stra.getEnableFlag()));
        this.setActionType(stra.getActionType());
        this.setCreId(stra.getCrelId());
        this.setCreDetail(stra.getCrelDetail());
    }

    @Override
    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getCreId() {
        return creId;
    }

    public void setCreId(String creId) {
        this.creId = creId;
    }

    public String getCreDetail() {
        return creDetail;
    }

    public void setCreDetail(String creDetail) {
        this.creDetail = creDetail;
    }
}
