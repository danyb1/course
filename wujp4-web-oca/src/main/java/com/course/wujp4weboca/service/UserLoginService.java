package com.course.wujp4weboca.service;

import com.course.wujp4weboca.constants.BaseConstants;
import com.course.wujp4weboca.domin.UserLogin;
import com.course.wujp4weboca.domin.UserLoginExample;
import com.course.wujp4weboca.dto.*;
import com.course.wujp4weboca.exception.BusinessException;
import com.course.wujp4weboca.exception.BusinessExceptionCode;
import com.course.wujp4weboca.mapper.UserLoginMapper;
import com.course.wujp4weboca.mapper.my.MyUserLoginMapper;
import com.course.wujp4weboca.service.auth.IUserAuthService;
import com.course.wujp4weboca.service.strategy.IStrategyDefineService;
import com.course.wujp4weboca.service.strategy.IStrategyValidateService;
import com.course.wujp4weboca.util.CopyUtil;
import com.course.wujp4weboca.util.SpringContextUtil;
import com.course.wujp4weboca.util.UuidUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserLoginService {

    @Resource
    private UserLoginMapper userLoginMapper;

    @Resource
    private MyUserLoginMapper myUserLoginMapper;

    @Autowired
    private IStrategyValidateService strategyValidateService;

    /**
     * 列表查询
     */
    public void list(PageDto pageDto) {
        PageHelper.startPage(pageDto.getPage(), pageDto.getSize());
        UserLoginExample userLoginExample = new UserLoginExample();
        List<UserLogin> userLoginList = userLoginMapper.selectByExample(userLoginExample);
        PageInfo<UserLogin> pageInfo = new PageInfo<>(userLoginList);
        pageDto.setTotal(pageInfo.getTotal());
        List<UserLoginDto> userLoginDtoList = CopyUtil.copyList(userLoginList, UserLoginDto.class);
        pageDto.setList(userLoginDtoList);
    }

    /**
     * 保存，id有值时更新，无值时新增
     */
    public void save(UserLoginDto userLoginDto) {
        UserLogin userLogin = CopyUtil.copy(userLoginDto, UserLogin.class);
        if (StringUtils.isEmpty(userLoginDto.getId())) {
            this.insert(userLogin);
        } else {
            this.update(userLogin);
        }
    }

    /**
     * 新增
     */
    private void insert(UserLogin userLogin) {
        Date now = new Date();
        userLogin.setId(UuidUtil.getShortUuid());
        userLoginMapper.insert(userLogin);
    }

    /**
     * 更新
     */
    private void update(UserLogin userLogin) {
        userLoginMapper.updateByPrimaryKey(userLogin);
    }

    /**
     * 删除
     */
    public void delete(String id) {
        userLoginMapper.deleteByPrimaryKey(id);
    }

    /**
     * 根据用户码查询用户信息
     * @param usercode
     * @return
     */
    public UserLoginDto getUserLoginByUsercode(String usercode){
        UserLoginDto userLoginDto=new UserLoginDto();
        UserLogin userLogin=myUserLoginMapper.getUserLoginByUsercode(usercode);
        if(userLogin!=null){
            System.out.println(myUserLoginMapper.getUserLoginByUsercode(usercode));
            BeanUtils.copyProperties(myUserLoginMapper.getUserLoginByUsercode(usercode),userLoginDto);

        }
        return userLoginDto;
    }

    /**
     * oca执行安全验证（UAA执行安全验证时，把此方法内容分两个接口执行）
     * @param userDto
     * @return
     */
    public UserLoginRepDto login(UserLoginReqDto userDto){

        //根据认证类型，获取授权实现
//        IUserAuthService authService = SpringContextUtil.getBean(userDto.getLoginType()+ BaseConstants.USER_AUTH_SERVICE_NAME);
//        String userCode = authService.getUserCode(userDto);
//        if(StringUtils.isEmpty(userCode)){
//            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
//        }
//        //登录用户状态、机构状态判断
//        UserLogin userLogin = this.checkUserState(userCode);
//        this.checkUserOrgState(userCode);

        //前置认证策略验证
        //UserLoginRepDto userLoginRepDto = this.beforeAuth(userDto, userLogin);
//        if(!userLoginRepDto.getResult()){
//            return userLoginRepDto;
//        }

        //认证信息验证
//        if(!authService.auth(userDto)) {
//            strategyValidateService.errorAuth(userDto, userDetail);
//            authService.authFailed(userDto);
//        }else{
//            strategyValidateService.successAuth(userDto, userDetail);
//            userLoginRepDto.setResult(true);
//        }
//        strategyValidateService.afterAuth(userDto, userDetail);
//        return userLoginRepDto;
        UserLogin userLogin1=myUserLoginMapper.getUserLoginByUsercode(userDto.getUserCode());
        UserLoginRepDto userLoginRepDto=new UserLoginRepDto();
        if(userLogin1.equals(null)){
            throw new BusinessException(BusinessExceptionCode.LOGIN_ERROR);
        }else{
            if(userLogin1.getLastLoginTime()==null){
                return null;
            }else{
                userLoginRepDto.setCode(userLogin1.getUserCode());
                return userLoginRepDto;
            }

        }
        //return null;
    }

    /**
     * 前置认证策略
     * @param userDto
     * @param userLogin
     * @return
     */
    private UserLoginRepDto beforeAuth(UserLoginReqDto userDto, UserLogin userLogin){
        UserLoginRepDto userLoginRepDto = new UserLoginRepDto();
        List<StrategyMessageDto> authDetails = strategyValidateService.beforeAuth(userDto, userLogin);
        if(authDetails != null){
            List<StrategyMessageDto> errorAuthDetails = authDetails.stream().filter(a->!BaseConstants.USER_STATE_WARNING.equals(a.getAction())).collect(Collectors.toList());
            if(!errorAuthDetails.isEmpty()){//包含非警告类策略时
                userLoginRepDto.setResult(false);
                userLoginRepDto.setStrategyMessage(errorAuthDetails);
            }else{
                userLoginRepDto.setStrategyMessage(authDetails);
            }
        }
        return userLoginRepDto;
    }

    /**
     * 校验用户的所属机构是否生效
     * @param userCode
     */
    private void checkUserOrgState(String userCode) {
    }

    /**
     * 判断用户的状态,校验用户是否生效
     * @param userCode
     * @return
     */
    private UserLogin checkUserState(String userCode) {
        UserLogin userLogin=myUserLoginMapper.getUserLoginByUsercode(userCode);
        log.info("获取的用户信息：{}",userLogin);
        if(BaseConstants.USER_STATUS_UNACTIVATED.equals(userLogin.getUserStatus())){
            log.info("[{}] account status [to be effective]", userLogin.getUserCode());
            throw new BusinessException(BusinessExceptionCode.USER_UNACTIVATED);
        }else if(BaseConstants.USER_STATUS_DISABLED.equals(userLogin.getUserStatus())){
            log.info("[{}] account status [invalid]",userLogin.getUserCode());
            throw new BusinessException(BusinessExceptionCode.USER_DISABLED);
        }else{
            return userLogin;
        }
    }
}
