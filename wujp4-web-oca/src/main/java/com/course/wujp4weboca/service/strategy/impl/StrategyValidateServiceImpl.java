package com.course.wujp4weboca.service.strategy.impl;

import com.course.wujp4weboca.constants.BaseConstants;
import com.course.wujp4weboca.domin.UserLogin;
import com.course.wujp4weboca.dto.StrategyMessageDto;
import com.course.wujp4weboca.dto.UserLoginReqDto;
import com.course.wujp4weboca.enums.AuthType;
import com.course.wujp4weboca.service.strategy.IStrategyDefineService;
import com.course.wujp4weboca.service.strategy.IStrategyInitService;
import com.course.wujp4weboca.service.strategy.IStrategyValidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class StrategyValidateServiceImpl implements IStrategyValidateService {

    @Autowired
    private IStrategyInitService strategyInitService;

    @Override
    public List<StrategyMessageDto> beforeAuth(UserLoginReqDto reqDto, UserLogin userLogin) {
        return this.execAuth(reqDto, userLogin, AuthType.BEFORE);
    }

    @Override
    public List<StrategyMessageDto> afterAuth(UserLoginReqDto reqDto, UserLogin userLogin) {
        return this.execAuth(reqDto, userLogin, AuthType.AFTER);
    }

    @Override
    public List<StrategyMessageDto> errorAuth(UserLoginReqDto reqDto, UserLogin userLogin) {
        return this.execAuth(reqDto, userLogin, AuthType.ERROR);
    }

    @Override
    public List<StrategyMessageDto> successAuth(UserLoginReqDto reqDto, UserLogin userLogin) {
        return this.execAuth(reqDto, userLogin, AuthType.SUCCESS);
    }

    @Override
    public List<StrategyMessageDto> passwdAuth(UserLoginReqDto reqDto, UserLogin userLogin) {
        return this.execAuth(reqDto, userLogin, AuthType.PASSWD);
    }

    /**
     * 执行认证策略
     * @param reqDto
     * @param userLogin
     * @param authType
     * @return
     */
    private List<StrategyMessageDto> execAuth(UserLoginReqDto reqDto, UserLogin userLogin, AuthType authType) {
        List<IStrategyDefineService> strategyDefines = this.loadStrategyDefine(reqDto.getSysId(), authType);
        if(strategyDefines != null && strategyDefines.size()>0){
            List<StrategyMessageDto> strategyMessageDtos = new ArrayList<>();
            for(IStrategyDefineService sds : strategyDefines){
                if(sds.isEnable()){
                    StrategyMessageDto strategyMessageDto = sds.validate(reqDto, userLogin);
                    if(strategyMessageDto != null){
                        strategyMessageDtos.add(strategyMessageDto);
                        //非警告类策略，终止策略验证
                        if (!BaseConstants.USER_STATE_WARNING.equals(strategyMessageDto.getAction())) {
                            break;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * 获取认证策略
     * @param sysId
     * @param authType
     * @return
     */
    private List<IStrategyDefineService> loadStrategyDefine(String sysId, AuthType authType) {
        Map<AuthType, List<IStrategyDefineService>> strategyDefines = strategyInitService.initStrategy(sysId);
        if(strategyDefines != null){
            return strategyDefines.get(authType);
        }
        return null;
    }
}
