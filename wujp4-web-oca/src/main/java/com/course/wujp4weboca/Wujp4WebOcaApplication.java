package com.course.wujp4weboca;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/24/ 16:47
 */
//@EnableEurekaClient
//@EnableDiscoveryClient
@SpringBootApplication
public class Wujp4WebOcaApplication {
    private static final Logger LOG = LoggerFactory.getLogger(Wujp4WebOcaApplication.class);

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Wujp4WebOcaApplication.class);
        Environment env = app.run(args).getEnvironment();
        LOG.info("启动成功！！");
        LOG.info("Wujp4WebOca地址: \thttp://127.0.0.1:{}", env.getProperty("server.port"));
    }
}
