package com.course.wujp4weboca.enums;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/28/ 14:28
 */
public enum LoginCode {
    LOGIN_FAIL(10000,"登录失败"),
    LOGIN_SUCCESS(10001,"登录成功");

    private Integer code;
    private String desc;

    LoginCode(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
