package com.course.wujp4weboca.enums;

public enum AuthType {
    BEFORE, AFTER, ERROR, SUCCESS, PASSWD
}
