package com.course.wujp4weboca.controller;

import com.course.wujp4weboca.constants.BusinessSuccessCode;
import com.course.wujp4weboca.dto.*;
import com.course.wujp4weboca.exception.BusinessException;
import com.course.wujp4weboca.exception.BusinessExceptionCode;
import com.course.wujp4weboca.service.UserLoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/24/ 17:08
 */
@RestController
@Slf4j
public class UserController {
//    @Resource
//    private UserService userService;

    @Resource
    private UserLoginService userLoginService;

    @Autowired
    private HttpServletRequest request;

    /**
     * 根据用户名查询用户信息
     * @param usercode
     * @return
     */
    @GetMapping(value="/user/getUser")
    public ResponseDto<UserLoginDto> getUserLoginByUsercode(@RequestParam(name ="usercode") String usercode){
        log.info("输入的名称：{}",usercode);
        //UserDto userDto1=userService.getUserDtoByUsername(username);
        ResponseDto<UserLoginDto> responseDto=new ResponseDto();
        try{
            UserLoginDto userDto=userLoginService.getUserLoginByUsercode(usercode);
            log.info("获取的结果：{}",userDto);
            if(userDto==null || userDto.getUserCode()==null){
                responseDto.setCode("400");
                responseDto.setSuccess(false);
                responseDto.setMessage("用户不存在");
            }else{
                responseDto.setCode("200");
                responseDto.setSuccess(true);
                responseDto.setMessage("用户存在");
                responseDto.setContent(userDto);
            }
        }catch (Exception e){
            responseDto.setCode("500");
            responseDto.setSuccess(false);
            responseDto.setMessage(e.getMessage());
        }
        return responseDto;
    }



    @PostMapping("/api/user/login")
    public ResultDto login(){
        System.out.println("输入用户名："+request.getParameter("userCode")+","+"输入的密码："+request.getParameter("password"));
        UserLoginReqDto userLoginReqDto=new UserLoginReqDto();
        String userCode=request.getParameter("userCode");
        String password=request.getParameter("password");
        userLoginReqDto.setUserCode(userCode);
        userLoginReqDto.setPassword(password);
        ResultDto resultDto = new ResultDto();
        try {
            userLoginReqDto.setUserCode(request.getParameter("userCode"));
            userLoginReqDto.setPassword(request.getParameter("password"));
            //userLoginReqDto.setLoginType(request.getParameter("loginType"));
            UserLoginRepDto userLoginRepDto = userLoginService.login(userLoginReqDto);
            if(userLoginRepDto!=null){
                resultDto.setCode(BusinessSuccessCode.LOGIN_SUCCESS.getCode());
                resultDto.setMessage(BusinessSuccessCode.LOGIN_SUCCESS.getDesc());
                resultDto.setData(userLoginReqDto);
            }else{
                resultDto.setCode(BusinessSuccessCode.FIRST_LOGIN.getCode());
                resultDto.setMessage(BusinessSuccessCode.FIRST_LOGIN.getDesc());
            }
        }catch (BusinessException e){
            UserLoginRepDto userLoginRepDto = new UserLoginRepDto();
            userLoginRepDto.setResult(false);
            userLoginRepDto.setCode(String.valueOf(e.getCode()));
            userLoginRepDto.setMessage(e.getMessage());
            resultDto.setData(userLoginRepDto);
        }catch (Exception all){
            resultDto.setCode(BusinessExceptionCode.LOGIN_ERROR.getCode());
            resultDto.setMessage(BusinessExceptionCode.LOGIN_ERROR.getDesc());
        }
        return resultDto;
    }
}
