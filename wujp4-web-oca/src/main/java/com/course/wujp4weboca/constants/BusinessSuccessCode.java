package com.course.wujp4weboca.constants;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/29/ 15:00
 */
public enum BusinessSuccessCode {

    LOGIN_SUCCESS("0000","登录成功"),
    FIRST_LOGIN("0001","首次登录");

    private String code;
    private String desc;

    BusinessSuccessCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
