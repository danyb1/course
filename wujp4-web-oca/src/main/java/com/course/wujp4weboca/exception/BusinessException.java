package com.course.wujp4weboca.exception;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/28/ 16:51
 */
public class BusinessException extends RuntimeException{

    private String code;
    private String message;

    public BusinessException(BusinessExceptionCode code) {
        super(code.getDesc());
        this.code = code.getCode();
        this.message = code.getDesc();
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 不写入堆栈信息，提高性能
     */
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }
}
