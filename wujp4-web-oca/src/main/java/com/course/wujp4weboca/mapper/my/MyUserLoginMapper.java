package com.course.wujp4weboca.mapper.my;

import com.course.wujp4weboca.domin.UserLogin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/25/ 11:29
 */
@Mapper
public interface MyUserLoginMapper {
    UserLogin getUserLoginByUsercode(@Param("userCode") String userCode);
}
