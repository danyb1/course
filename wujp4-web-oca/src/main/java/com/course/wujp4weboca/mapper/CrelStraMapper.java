package com.course.wujp4weboca.mapper;

import com.course.wujp4weboca.domin.CrelStra;
import com.course.wujp4weboca.domin.CrelStraExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CrelStraMapper {
    long countByExample(CrelStraExample example);

    int deleteByExample(CrelStraExample example);

    int deleteByPrimaryKey(String id);

    int insert(CrelStra record);

    int insertSelective(CrelStra record);

    List<CrelStra> selectByExampleWithBLOBs(CrelStraExample example);

    List<CrelStra> selectByExample(CrelStraExample example);

    CrelStra selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CrelStra record, @Param("example") CrelStraExample example);

    int updateByExampleWithBLOBs(@Param("record") CrelStra record, @Param("example") CrelStraExample example);

    int updateByExample(@Param("record") CrelStra record, @Param("example") CrelStraExample example);

    int updateByPrimaryKeySelective(CrelStra record);

    int updateByPrimaryKeyWithBLOBs(CrelStra record);

    int updateByPrimaryKey(CrelStra record);
}