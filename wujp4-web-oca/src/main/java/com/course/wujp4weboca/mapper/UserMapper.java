package com.course.wujp4weboca.mapper;

import com.course.wujp4weboca.domin.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/24/ 16:56
 */
@Mapper
public interface UserMapper {
    User getUserByUsername(@Param("username") String username);
}
