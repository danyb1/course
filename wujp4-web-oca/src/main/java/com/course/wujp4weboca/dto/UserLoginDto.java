package com.course.wujp4weboca.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class UserLoginDto {

    /**
     * 用户登录编号
     */
    private String id;

    /**
     * 用户登录码
     */
    private String userCode;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 用户密码
     */
    private String userPassword;

    /**
     * 用户所属机构编号
     */
    private String orgId;

    /**
     * 用户的创建者
     */
    private String userCreator;

    /**
     * 用户最近登录时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date lastLoginTime;

    /**
     * 用户最近一次修改密码时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date lastEditPasswordTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getUserCreator() {
        return userCreator;
    }

    public void setUserCreator(String userCreator) {
        this.userCreator = userCreator;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Date getLastEditPasswordTime() {
        return lastEditPasswordTime;
    }

    public void setLastEditPasswordTime(Date lastEditPasswordTime) {
        this.lastEditPasswordTime = lastEditPasswordTime;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userCode=").append(userCode);
        sb.append(", userName=").append(userName);
        sb.append(", userPassword=").append(userPassword);
        sb.append(", orgId=").append(orgId);
        sb.append(", userCreator=").append(userCreator);
        sb.append(", lastLoginTime=").append(lastLoginTime);
        sb.append(", lastEditPasswordTime=").append(lastEditPasswordTime);
        sb.append("]");
        return sb.toString();
    }

}