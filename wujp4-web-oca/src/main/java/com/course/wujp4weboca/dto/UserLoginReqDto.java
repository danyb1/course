package com.course.wujp4weboca.dto;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/28/ 14:31
 */
public class UserLoginReqDto implements Serializable {
    /**
     * 系统编号
     */
    private String sysId;
    /**
     * 登录类型（密码：password）
     */
    private String loginType;
    /**
     * 登录号
     */
    private String userCode;
    /**
     * 登录密码
     */
    private String password;
    /**
     * 授权信息
     */
    private String authCode;
    /**
     * 扩展信息
     */
    private Map<String, Object> extendsInfo;

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public Map<String, Object> getExtendsInfo() {
        return extendsInfo;
    }

    public void setExtendsInfo(Map<String, Object> extendsInfo) {
        this.extendsInfo = extendsInfo;
    }
}
