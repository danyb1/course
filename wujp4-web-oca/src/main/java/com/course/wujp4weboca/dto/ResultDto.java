package com.course.wujp4weboca.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/28/ 14:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultDto<T> {
    private String code;
    private String message;
    private T data;

    public ResultDto(String code, String message) {
        this(code, message, null);
    }
}
