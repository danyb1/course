package com.course.wujp4weboca.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/29/ 17:36
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CrelStraDto implements Serializable {

    private String id;

    private String sysId;

    private String crelName;

    private String enableFlag;

    private String actionType;

    private String lastChgUsr;

    private String lastChgDt;

    private String crelDetail;
}
