/**
 * 
 */
package com.yzp.filter;


import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.yzp.dto.TokenInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/18 19:17
 */
@Component
public class SessionTokenFilter extends ZuulFilter {

    private static RestTemplate restTemplate = new RestTemplate();
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 2;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        TokenInfo token = (TokenInfo)RequestContext.getCurrentContext().getRequest().getSession().getAttribute("token");
        if(Objects.nonNull(token)){
            String access_token = token.getAccess_token();
            if(!token.isExpires()){
                String oauthServiceUrl = "http://gateway.imooc.com:9070/token/oauth/token";

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
				headers.setBasicAuth("admin", "123456");

				MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
				params.add("grant_type", "refresh_token");
				params.add("refresh_token", token.getRefresh_token());

				HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
                RequestContext requestContext = RequestContext.getCurrentContext();
				try {
					ResponseEntity<TokenInfo> newToken = restTemplate.exchange(oauthServiceUrl, HttpMethod.POST, entity, TokenInfo.class);
					requestContext.getRequest().getSession().setAttribute("token", newToken.getBody().init());
                    access_token = newToken.getBody().getAccess_token();
				} catch (Exception e) {

                    requestContext.setSendZuulResponse(false);
					requestContext.setResponseStatusCode(500);
					requestContext.setResponseBody("{\"message\":\"refresh fail\"}");
					requestContext.getResponse().setContentType("application/json");
				}
			}
            RequestContext.getCurrentContext().addZuulRequestHeader("Authorization", "bearer "+access_token);
        }


        return null;
    }
}
