package com.yzp.exception;

import com.yzp.dto.ResponseDto;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/18 19:26
 */
@ControllerAdvice
public class ValidatorException {
    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseDto exceptionHandle(HttpClientErrorException err){
        ResponseDto responseDto = new ResponseDto();
        responseDto.setSuccess(false);
        responseDto.setMessage(err.getMessage());
        return responseDto;
    }
}
