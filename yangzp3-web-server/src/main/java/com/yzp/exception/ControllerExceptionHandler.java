package com.yzp.exception;

import com.yzp.dto.ResponseDto;
import com.yzp.dto.TokenInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/18 19:27
 */
@ControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler(value = BusinessException.class)
    public ResponseDto exceptionHandle(BusinessException err){
        //HashMap<String, Object> map = new HashMap<>();
        ResponseDto responseDto = new ResponseDto();
        responseDto.setMessage(BusinessExceptionCode.HAS_ERROR.getDesc());
        responseDto.setSuccess(false);
        return responseDto;
    }
}
