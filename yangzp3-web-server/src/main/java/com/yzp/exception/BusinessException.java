package com.yzp.exception;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/18 19:25
 */
public class BusinessException extends RuntimeException {
    private BusinessExceptionCode code;

    public BusinessException(BusinessExceptionCode code){
        this.code = code;
    }

    public BusinessExceptionCode getCode(){
        return code;
    }

    public void setCode(BusinessExceptionCode code){
        this.code = code;
    }

    @Override
    public Throwable fillInStackTrace(){
        return this;
    }
}
