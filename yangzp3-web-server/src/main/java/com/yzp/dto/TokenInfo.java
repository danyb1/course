package com.yzp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/18 19:24
 */
@Data
public class TokenInfo {
    private String access_token;
    private String refresh_token;
    private long expires_in;
    private String token_type;
    private String scope;

    private LocalDateTime expireTime;

    public TokenInfo init(){
        expireTime = LocalDateTime.now().plusSeconds(expires_in-3);
        return  this;
    }

    @JsonIgnore
    public boolean isExpires(){
        return expireTime.isBefore(LocalDateTime.now());
    }
}
