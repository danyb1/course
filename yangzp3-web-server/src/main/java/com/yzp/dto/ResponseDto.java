package com.yzp.dto;

import lombok.Data;
import org.springframework.util.StringUtils;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/18 19:24
 */

public class ResponseDto<T> {
    private T content;
    private boolean success;
    private String message;
    private String code;

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ResponseDto{" +
                "content=" + content +
                ", success=" + success +
                ", message='" + message + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
