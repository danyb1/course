package com.yzp.dto;

import lombok.Data;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/18 19:24
 */
@Data
public class Credentials {
    private String username;
    private String password;
}
