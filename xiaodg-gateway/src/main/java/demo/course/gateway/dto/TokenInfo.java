package demo.course.gateway.dto;

import java.util.Arrays;
import java.util.Date;

public class TokenInfo {
    private Boolean active;
    private String client_id;
    // 访问权限
    private String[] scope;
    private String user_name;
    private String[] aud;
    /**
     * 过期时间
     * */
    private Date exp;

    private String[] authorities;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String[] getScope() {
        return scope;
    }

    public void setScope(String[] scope) {
        this.scope = scope;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String[] getAud() {
        return aud;
    }

    public void setAud(String[] aud) {
        this.aud = aud;
    }

    public Date getExp() {
        return exp;
    }

    public void setExp(Date exp) {
        this.exp = exp;
    }

    public String[] getAuthorities() {
        return authorities;
    }

    public void setAuthorities(String[] authorities) {
        this.authorities = authorities;
    }

    @Override
    public String toString() {
        return "TokenInfo{" +
                "active=" + active +
                ", client_id='" + client_id + '\'' +
                ", scope=" + Arrays.toString(scope) +
                ", user_name='" + user_name + '\'' +
                ", aud=" + Arrays.toString(aud) +
                ", exp=" + exp +
                ", authorities=" + Arrays.toString(authorities) +
                '}';
    }
}
