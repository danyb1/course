package demo.course.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import demo.course.gateway.dto.TokenInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 授权过滤器
 */
@Component
public class AuthorizationFilter extends ZuulFilter {

    private static final Logger logger = LoggerFactory.getLogger(AuthorizationFilter.class);


    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 3;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        logger.info("3. 开始授权.");
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        if (isNeedAuth(request)) {
            TokenInfo tokenInfo = (TokenInfo)request.getAttribute("tokenInfo");
            if (tokenInfo != null && tokenInfo.getActive()) {
                if (!hasPermission(tokenInfo, request)) {
                    logger.info("audit log update failed 403");
                    handleError(403, requestContext);
                }
            } else {
                // 如果是调用认证服务的，直接过
                if (!StringUtils.startsWith(request.getRequestURI(), "/token")) {
                    logger.info("audit log update failed 401");
                    handleError(401, requestContext);
                }
            }
        }
        return null;
    }

    private boolean hasPermission(TokenInfo tokenInfo, HttpServletRequest request) {
        // 根据实际业务作调整
//        return RandomUtils.nextInt() % 2 == 0;
        return true;
    }

    private void handleError(int responseCode, RequestContext requestContext) {
        HttpServletResponse response = requestContext.getResponse();
        response.setStatus(responseCode);
        response.setContentType("application/json");
        requestContext.setResponseBody("{\"message\": \"auth failed\"}");
        // 不向业务服务继续调用，直接返回给客户端
        requestContext.setSendZuulResponse(false);
    }

    private boolean isNeedAuth(HttpServletRequest request) {
        // 根据实际业务作调整
        return true;
    }
}
