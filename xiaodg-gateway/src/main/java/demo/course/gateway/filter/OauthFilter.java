package demo.course.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import demo.course.gateway.dto.TokenInfo;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * 继承ZUUL Filter
 */
@Component
public class OauthFilter extends ZuulFilter {
    private static final Logger logger = LoggerFactory.getLogger(OauthFilter.class);

    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public String filterType() {
        // 支持四个字符串, pre, post, error, route
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * 写业务逻辑的地方
     *
     * @return
     * @throws ZuulException
     */
    @Override
    public Object run() throws ZuulException {
        logger.info("1. 开始认证");
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        if (StringUtils.startsWith(request.getRequestURI(), "/token")) {
            // 发往认证服务器的请求，直接过
            return null;
        }
        // 发往业务服务的请求
        String authorization = request.getHeader("Authorization");
        if (StringUtils.isBlank(authorization)) {
            // 如果没有带认证直接过，当然实际上这里是有问题的，应该根据实际情况进行优化
            return null;
        }
        boolean isBearerToken = StringUtils.startsWith(authorization, "Bearer ");
        if (!isBearerToken) {
            // 不是Bearer类型的token，放过
            return null;
        }
        // 校验Bearer Token
        try {
            TokenInfo tokenInfo = getTokenInfo(authorization);
            request.setAttribute("tokenInfo", tokenInfo);
        } catch (Exception e) {
            logger.error("get token failed:", e);
        }

        return null;
    }

    /**
     * 从UAA获取token信息
     * @param authorization
     * @return
     */
    private TokenInfo getTokenInfo(String authorization) {
        String token = StringUtils.substringAfter(authorization, "Bearer ");
        String oauthServiceUrl = "http://localhost:9070/token/oauth/check_token";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        // 需要先在认证服务器上面配置网关应用
        httpHeaders.setBasicAuth("gateway", "123456");
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("token", token);

        HttpEntity<MultiValueMap<String, String>> multiValueMapHttpEntity = new HttpEntity<>(params, httpHeaders);

        ResponseEntity<TokenInfo> response = restTemplate.exchange(oauthServiceUrl, HttpMethod.POST, multiValueMapHttpEntity, TokenInfo.class);

        TokenInfo body = response.getBody();
        if (body != null) {
            logger.info("token info is: " + body.toString());
        }

        return body;
    }
}
