//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.springframework.security.web.authentication.ui;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.course.oauth.config.Oauth2LogoutSuccessHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;

public class DefaultLogoutPageGeneratingFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(DefaultLogoutPageGeneratingFilter.class);


    private RequestMatcher matcher = new AntPathRequestMatcher("/logout", "GET");
    private Function<HttpServletRequest, Map<String, String>> resolveHiddenInputs = (request) -> Collections.emptyMap();

    public DefaultLogoutPageGeneratingFilter() {
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (this.matcher.matches(request)) {
            this.renderLogout(request, response);
        } else {
            filterChain.doFilter(request, response);
        }

    }

    private void renderLogout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String redirectUri = request.getParameter("redirect_uri");
        logger.info("登出后需要跳转到页面(redirectUri): " + redirectUri);
        String page = "<!DOCTYPE html>\n<html lang=\"en\">\n  <head>\n    " +
                "<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n " +
                "   <meta name=\"description\" content=\"\">\n    <meta name=\"author\" content=\"\">\n    <title>退出</title>\n  " +
                "  <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css\" rel=\"stylesheet\"" +
                " integrity=\"sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M\" crossorigin=\"anonymous\">\n  " +
                "  <link href=\"https://getbootstrap.com/docs/4.0/examples/signin/signin.css\" rel=\"stylesheet\" crossorigin=\"anonymous\"/>\n " +
                " </head>\n  <body>\n     <div class=\"container\">\n    " +
                // 注意，action写的是从网关过来的地址，否则会变成gateway/logout，而这个地址是不存在的
                "  <form id=\"logoutForm\" class=\"form-signin\" method=\"post\" action=\"" + request.getContextPath() + "/token/logout\">\n       " +
                " <h2 class=\"form-signin-heading\">Are you sure you want to log out?</h2>\n" + this.renderHiddenInputs(request) + "      " +
                //" | <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Log Out</button>\n     " +
                // 处理自定义的属性
                "<input name=\"redirect_uri\" type=\"hidden\" value=\""+ redirectUri +"\"/>" +
                " </form>\n  " +
                "  </div>\n " +
                // 自定义的JS脚本,使得表单自动提交
                " <script>document.getElementById('logoutForm').submit();</script>\n" +
                " </body>\n</html>";

        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().write(page);
    }

    public void setResolveHiddenInputs(Function<HttpServletRequest, Map<String, String>> resolveHiddenInputs) {
        Assert.notNull(resolveHiddenInputs, "resolveHiddenInputs cannot be null");
        this.resolveHiddenInputs = resolveHiddenInputs;
    }

    private String renderHiddenInputs(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        Iterator var3 = ((Map)this.resolveHiddenInputs.apply(request)).entrySet().iterator();

        while(var3.hasNext()) {
            Entry<String, String> input = (Entry)var3.next();
            sb.append("<input name=\"").append((String)input.getKey()).append("\" type=\"hidden\" value=\"").append((String)input.getValue()).append("\" />\n");
        }

        return sb.toString();
    }
}
