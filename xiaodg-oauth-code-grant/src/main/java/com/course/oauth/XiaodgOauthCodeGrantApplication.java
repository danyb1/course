package com.course.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class XiaodgOauthCodeGrantApplication {

	public static void main(String[] args) {
		SpringApplication.run(XiaodgOauthCodeGrantApplication.class, args);
	}

}
