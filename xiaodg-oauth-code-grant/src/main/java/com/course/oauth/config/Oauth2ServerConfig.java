package com.course.oauth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import javax.sql.DataSource;
import java.security.KeyPair;

/**
 * 认证服务器配置
 */
@Configuration
@EnableAuthorizationServer
public class Oauth2ServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * 第1步配置
     * 配置客户端
     * <p>
     * 配置哪些客户端可以访问哪些应用
     *
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        // 可以从数据库中加载
        // clients.jdbc(DataSource);

        // 下面是手动写死的
        clients
                .inMemory()
                .withClient("orderApp") // 客户端
                .secret(passwordEncoder.encode("123456"))
                .scopes("read", "write") // 访问权限
                .accessTokenValiditySeconds(3600)
                .resourceIds("order-server") // 应用（资源服务器），可以填写多个
                .authorizedGrantTypes("password")
                .and()
                .withClient("orderService")
                .secret(passwordEncoder.encode("123456"))
                .scopes("read")
                .accessTokenValiditySeconds(3600)
                .resourceIds("order-server")
                .authorizedGrantTypes("password")
                .and()
                .withClient("gateway") // 配置网关，供网关调用
                .secret(passwordEncoder.encode("123456"))
                .scopes("read", "write")
                .accessTokenValiditySeconds(3600)
                .resourceIds("gateway-server")
                .authorizedGrantTypes("password", "refresh_token")
                .and()
                .withClient("admin") // 配置前端应用
                .secret(passwordEncoder.encode("123456"))
                .scopes("read", "write")
                .accessTokenValiditySeconds(3600)
                .refreshTokenValiditySeconds(19200)
                .resourceIds("admin-server")
                .authorizedGrantTypes("password", "authorization_code", "refresh_token")
                // 授权码模式需要填写应用的回调地址
                .redirectUris("http://localhost:9999/oauth/callback")
                // 自动授权，用户无需在页面上选择授权
                .autoApprove(true);
    }

    // 把token存储在Redis中的配置
//    @Bean
//    public TokenStore tokenStore() {
//        RedisTokenStore redisTokenStore = new RedisTokenStore();
//        return redisTokenStore;
//    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtTokenEnhancer());
    }

    private JwtAccessTokenConverter jwtTokenEnhancer() {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        // 注意，signingKey的作用是：确保信息没有被篡改过，而不是保证JWT的内容是加密的
        // 签名key，验证的人需要使用这个key进行验证，通过一个接口暴露这个key出去，但这个接口需要先认证，配置参考下面configure(AuthorizationServerSecurityConfigurer security)
        // jwtAccessTokenConverter.setSigningKey("123456");
        // 使用证书作为签名key
        KeyPair keyPair = new KeyStoreKeyFactory(new ClassPathResource("xiaodg.key"), "123456".toCharArray())
                .getKeyPair("xiaodg");
        jwtAccessTokenConverter.setKeyPair(keyPair);
        return jwtAccessTokenConverter;
    }


    /**
     * 第2步配置
     * 配置校验用户信息
     *
     * @param endpoints
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        // 这个userDetailsServic是给刷新token使用的
        endpoints.userDetailsService(userDetailsService);
        // 注意，authenticationManager中的userDetailsServics是给验证用户信息使用的，这个跟上面比较有点迷
        endpoints.authenticationManager(authenticationManager);
        // 如果设置了token的存储位置为redis或mysql需要使用下面的代码
        // endpoints.tokenStore(tokenStore())
        endpoints.tokenStore(tokenStore())
                .tokenEnhancer(jwtTokenEnhancer());
    }

    /**
     * 第5步配置
     * <p>
     * 在什么情况下进行身份认证
     *
     * @param security
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        // 校验token之前，这个请求一定是经过身份认证了的，即使用了上面的客户端用户和密码的
        // security.tokenKeyAccess("isAuthenticated()")表示访问获取SigningKey的接口先要认证
        security.tokenKeyAccess("isAuthenticated()").checkTokenAccess("isAuthenticated()");
    }
}
