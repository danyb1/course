package com.course.oauth.config;

import io.micrometer.core.instrument.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登出的处理
 */
@Component
public class Oauth2LogoutSuccessHandler implements LogoutSuccessHandler {

    private static final Logger logger = LoggerFactory.getLogger(Oauth2LogoutSuccessHandler.class);


    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        String redirectUri = request.getParameter("redirect_uri");
        if (StringUtils.isNotBlank(redirectUri)) {
            // 登出后跳转到业务系统的某个地址，这个地址是业务系统登出的时候传递过来的
            response.sendRedirect(redirectUri);
        } else {
            logger.error("没有获取到redirectUri: " + redirectUri);
        }

    }
}
