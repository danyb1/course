package com.course.wujp4webgateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.core.env.Environment;

/**
 * @Author wjp
 * @Email: wujp4@yusys.com.cn
 * @Date: 2020/09/21/ 16:19
 */
@SpringBootApplication(scanBasePackages = "com.course")
@EnableZuulProxy
@EnableEurekaClient
@EnableDiscoveryClient
public class Wujp4WebGatewayApplication {

    private static final Logger LOG = LoggerFactory.getLogger(Wujp4WebGatewayApplication.class);

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Wujp4WebGatewayApplication.class);
        Environment env = app.run(args).getEnvironment();
        LOG.info("启动成功！！");
        LOG.info("Wujp4WebGateway地址: \thttp://127.0.0.1:{}", env.getProperty("server.port"));
    }
}
