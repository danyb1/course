package com.yzp;

import com.alibaba.fastjson.JSON;
import com.yzp.mapper.UserMapper;
import com.yzp.entity.MyUser;
import lombok.extern.slf4j.Slf4j;


import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Optional;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/25 9:38
 */
@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class test {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;
    @Test
    public void testData(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        Optional<MyUser> yaoyao = Optional.ofNullable(mapper.loadUserByUsername("yaoyao"));
        if(yaoyao.isPresent()){
            System.out.println(yaoyao.get());
        }
    }

/*    @Test
    public void testHttp(){
        String url = "http://localhost:9002/getUser?username=yaoyao";
        //DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        try{
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            String s = EntityUtils.toString(response.getEntity(),"utf-8");
            System.out.println("s====" + s);
           //User o = (User)JSON.parseObject(s, User.class);
            //Object o = JSON.parseObject(s, User.class);
            User user = JSON.parseObject(s, User.class);
            System.out.println(user);
        }catch (Exception e){
        }
    }*/

    @Test
    public void testRestTemplate() throws UnsupportedEncodingException {
        RestTemplate restTemplate = new RestTemplate();
        String username = "yaoyao";
        //String url = "http://localhost:9002/getUser?username=yaoyao";
        String url = "http://localhost:9002/getUser?username=" + URLEncoder.encode(username,"UTF-8");
        String s = restTemplate.getForObject(url, String.class);
        log.info("RestTemplate请求得到的字符串为："+s);
        MyUser user = JSON.parseObject(s, MyUser.class);
        //System.out.println(user);
        log.info("JSON反序列化得到的user实体类为："+user);

        //LinkedMultiValueMap<String, String> map = new LinkedMultiValueMap<>();
       // map.add("username",username);
        /*HttpHeaders headers = new HttpHeaders();
        //headers.setBasicAuth(null,null);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        //headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        //HttpEntity<LinkedMultiValueMap<String, String>> httpEntity = new HttpEntity<>(map, headers);
        //ResponseEntity<User> exchange = restTemplate.exchange(url, HttpMethod.GET, httpEntity, User.class);
        ResponseEntity<User> exchange = restTemplate.exchange(url, HttpMethod.GET, null, User.class);
        String s = exchange.getBody().toString();*/
//        User user = JSON.parseObject(s, User.class);
//        System.out.println(user);

    }
    @Test
    public void testRestTemplate1(){
        RestTemplate restTemplate = new RestTemplate();
        String username = "yaoyao";
        HashMap<String, String> map = new HashMap<>();
        //MultiValueMap map = new LinkedMultiValueMap();
        map.put("username",username);
        String url = "http://localhost:9002/getUser?username={username}";
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class,map);
        System.out.println(forEntity.getBody());
        //log.info("RestTemplate请求得到的字符串为："+s);
        MyUser user = JSON.parseObject(forEntity.getBody().toString(), MyUser.class);
        //System.out.println(user);
        log.info("JSON反序列化得到的user实体类为："+user);
    }
    @Test
    public void testRestTemplate2() {
        RestTemplate restTemplate = new RestTemplate();
        String username = "yaoyao";
        String url = "http://localhost:9002/getUser";
        LinkedMultiValueMap<String, String> map = new LinkedMultiValueMap<>();
         map.add("username",username);
        HttpHeaders headers = new HttpHeaders();
        //headers.setBasicAuth(null,null);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        log.info("HttpHeaders的当前内容为："+headers.toString());
        //headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<LinkedMultiValueMap<String, String>> httpEntity = new HttpEntity<>(map, headers);
        //ResponseEntity<User> exchange = restTemplate.exchange(url, HttpMethod.GET, httpEntity, User.class);
        log.info("当前HttpEntity的返回值为："+httpEntity.getBody().toString());
        ResponseEntity<MyUser> exchange = restTemplate.exchange(url, HttpMethod.POST, httpEntity, MyUser.class);
        String s = exchange.getBody().toString();
        System.out.println("返回的user值为："+s);
    }

}
