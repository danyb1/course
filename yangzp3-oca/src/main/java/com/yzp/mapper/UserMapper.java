package com.yzp.mapper;

import com.yzp.entity.MyUser;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.mybatis.spring.annotation.MapperScan;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/22 19:19
 */
@Mapper
public interface UserMapper {

    @Select("select * from user where username = #{username}")
    MyUser loadUserByUsername(String username);


}







