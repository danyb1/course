package com.yzp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/22 19:22
 */
@Configuration
public class BCryptPasswordEncoderConfig {
    BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
