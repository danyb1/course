package com.yzp.validate;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/25 16:36
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Constraint(validatedBy = UsernameValidator.class)
public @interface Username {
    String message() default "用户名格式不对";
    Class<?>[] group() default {};
    Class<? extends Payload>[] payload() default {};
}


















