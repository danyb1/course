package com.yzp.validate;

import com.yzp.entity.MyUser;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/25 16:23
 */
public class PasswordValidator implements ConstraintValidator<Password,Object> {

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        String str = (String)o;

        return o != null && str.length()>0 ;
    }
}
