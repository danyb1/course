package com.yzp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/22 17:59
 */
@SpringBootApplication
public class OcaApp {
    public static void main(String[] args) {
        SpringApplication.run(OcaApp.class,args);
    }
}
