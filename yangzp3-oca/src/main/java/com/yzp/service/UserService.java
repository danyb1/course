package com.yzp.service;

import com.yzp.entity.MyUser;
import com.yzp.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/22 19:25
 */
@Service
public class UserService {
    @Autowired
    private UserMapper  userMapper;

    public MyUser loadUserByUsername(String username){
        return userMapper.loadUserByUsername(username);
    }
}
