package com.yzp.controller;

import com.yzp.enums.ErrorConstants;
import com.yzp.exception.UserException;
import com.yzp.service.UserService;
import com.yzp.entity.MyUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


import javax.servlet.http.HttpServletRequest;
import javax.sound.sampled.Line;
import javax.validation.Valid;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/22 19:23
 */
@Slf4j
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @RequestMapping("/validate")
    public boolean validate(String username,String password){
        MyUser user = userService.loadUserByUsername(username);
        if(Objects.nonNull(user)){

            if(encoder.matches(password,user.getPassword())){
                return true;
            }else{
                throw  new UserException(ErrorConstants.USER_PASSWORD_ERROR.getMsg());
            }
        }else{
            throw  new UserException(ErrorConstants.USER_PASSWORD_ERROR.getMsg());
        }
        //return false;
    }

    @RequestMapping("/getUser")
    @ResponseBody
    public MyUser getUser(@RequestParam String username){
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        Map<String, String[]> map = request.getParameterMap();
        Set<Map.Entry<String, String[]>> entries = map.entrySet();
        Iterator<Map.Entry<String, String[]>> iterator = entries.iterator();
        while(iterator.hasNext()){
            Map.Entry<String, String[]> next = iterator.next();
            System.out.println("Request请求的Key："+next.getKey()+
                    "，  Request请求的Value："+next.getValue());
        }
        MyUser user = userService.loadUserByUsername(username);
        if(user == null){
            throw new UserException(ErrorConstants.USER_NOT_FOUND.getMsg());
        }
        System.out.println(user.toString());
        return user;
    }
}
