package com.yzp.enums;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/22 19:33
 */
public enum  ErrorConstants {
    USER_NOT_FOUND(1000,"用户名不存在"),
    USER_PASSWORD_ERROR(1001,"密码错误");
    private int code;
    private String msg;

    ErrorConstants(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
