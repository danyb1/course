package com.yzp.entity;

import lombok.Data;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/25 16:19
 */
@Data
public class ResponseDto<T> {
    private T content;
    private String msg;
    private int code;

}
