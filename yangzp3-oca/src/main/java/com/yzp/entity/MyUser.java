package com.yzp.entity;

import com.yzp.validate.Password;
import com.yzp.validate.Username;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import reactor.util.annotation.NonNull;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 系统用户表(AdminSmUser)实体类
 *
 * @author makejava
 * @since 2020-09-22 18:25:52
 */
@Data
public class MyUser implements Serializable {
    private static final long serialVersionUID = -68224585251723869L;
   /* @NonNull*/
    private long id;

    private String name;
    /*@NotEmpty(message = "密码不能为空")
    @Password*/
    private String password;

    private String permissions;
    /*@Username
    @NotBlank(message = "用户名不能为空")
    @Length(min = 1,max = 255,message = "密码长度不能太大，也不能太小")*/
    private String username;

    private LocalDateTime dateTime;


}