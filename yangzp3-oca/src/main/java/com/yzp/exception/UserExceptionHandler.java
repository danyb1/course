package com.yzp.exception;

import com.yzp.entity.ResponseDto;
import com.yzp.enums.ErrorConstants;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/25 16:17
 */
@ControllerAdvice
public class UserExceptionHandler {

    @ExceptionHandler(value = {UserException.class })
    public ResponseDto handle(UserException err){
        ResponseDto<UserException> responseDto = new ResponseDto<>();
        responseDto.setMsg(ErrorConstants.USER_NOT_FOUND.getMsg());
        return responseDto;
    }
}














