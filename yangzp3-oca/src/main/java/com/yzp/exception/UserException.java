package com.yzp.exception;

/**
 * @author yzp
 * @version 1.0
 * @date 2020/9/22 19:36
 */

public class UserException extends RuntimeException {
    private String msg;
    public UserException(String msg){
        this.msg = msg;
    }
}
