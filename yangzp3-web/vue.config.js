module.exports = {
    devServer: {
        disableHostCheck: true,
        proxy: {
            '/yuspwebserver': {
                target: 'http://admin.imooc.com:7004',
                changeOrigin: true,
                // pathRewrite: {
                //     '^/yuspwebserver': ''
                // }
            }
        }
    }
}