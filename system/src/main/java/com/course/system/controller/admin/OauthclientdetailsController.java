package com.course.system.controller.admin;

import com.course.server.dto.OauthclientdetailsDto;
import com.course.server.dto.PageDto;
import com.course.server.dto.ResponseDto;
import com.course.server.service.OauthclientdetailsService;
import com.course.server.util.ValidatorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author danyu
 */
@RestController
@RequestMapping("/admin/oauthclientdetails")
public class OauthclientdetailsController {

    private static final Logger LOG = LoggerFactory.getLogger(OauthclientdetailsController.class);
    public static final String BUSINESS_NAME = "客户端oauth明细表";

    @Resource
    OauthclientdetailsService oauthclientdetailsService;

    /**
     * 列表查询
     */
    @PostMapping("/list")
    public ResponseDto oauthclientdetails(@RequestBody PageDto pageDto) {
        oauthclientdetailsService.list(pageDto);
        ResponseDto responseDto = new ResponseDto();
        responseDto.setContent(pageDto);
        return responseDto;
    }

    /**
     * 保存，id有值时更新，无值时新增
     */
    @PostMapping("/save")
    public ResponseDto save(@RequestBody OauthclientdetailsDto oauthclientdetailsDto) {
        // 保存校验
        ValidatorUtil.require(oauthclientdetailsDto.getClientId(), "客户端id");
        ValidatorUtil.length(oauthclientdetailsDto.getClientId(), "客户端id", 1, 256);
        ValidatorUtil.length(oauthclientdetailsDto.getResourceIds(), "资源ids", 1, 256);
        ValidatorUtil.length(oauthclientdetailsDto.getClientSecret(), "客户端密码", 1, 256);
        ValidatorUtil.length(oauthclientdetailsDto.getScope(), "客户端权限", 1, 256);
        ValidatorUtil.length(oauthclientdetailsDto.getAuthorizedGrantTypes(), "授权类型", 1, 256);
        ValidatorUtil.length(oauthclientdetailsDto.getWebServerRedirectUri(), "重定向url", 1, 256);
        ValidatorUtil.length(oauthclientdetailsDto.getAuthorities(), "用户权限", 1, 256);
        ValidatorUtil.length(oauthclientdetailsDto.getAdditionalInformation(), "附加信息", 1, 4096);
        ValidatorUtil.length(oauthclientdetailsDto.getAutoapprove(), "autoapprove", 1, 256);

        oauthclientdetailsService.save(oauthclientdetailsDto);
        ResponseDto responseDto = new ResponseDto();
        responseDto.setContent(oauthclientdetailsDto);
        return responseDto;
    }

    /**
     * 删除
     */
    @DeleteMapping("/delete/{id}")
    public ResponseDto delete(@PathVariable String id) {
        ResponseDto responseDto = new ResponseDto();
        oauthclientdetailsService.delete(id);
        return responseDto;
    }
}
